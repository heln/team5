package edu.ntnu.idatt2001.domain;

import edu.ntnu.idatt2001.Models.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

class PlayerTest {
    private String name;
    private int health;
    private int score;
    private int gold;
    private List<String> inventory;
    private Player player;

    @BeforeEach
    void generateInformation() {
        inventory = new ArrayList<>();
        inventory.add("item");
        player = new Player("name", 5, 5, 5, inventory);

        this.name = "name";
        this.health = 5;
        this.score = 5;
        this.gold = 5;
    }

    @Nested
    public class ConstructorTests {

        @Test
        @DisplayName("Creating a player object wil valid input")
        public void constructorTest(){
            assertEquals(name, player.getName());
            assertEquals(health, player.getHealth());
            assertEquals(score, player.getScore());
            assertEquals(gold, player.getGold());
            assertEquals(inventory, player.getInventory());
        }

        @Test
        @DisplayName("Creating a player object with negative health input should become 0")
        public void testWhenHealthIsBelowZero() {
            player.addHealth(-7);
            assertEquals(0, player.getHealth());
        }

        @Test
        @DisplayName("Creating a player object with blank name input should throw IllegalArgumentException")
        public void testWhenNameIsEmpty() {
            assertThrows(IllegalArgumentException.class, () -> new Player("  ", 0, 0 ,0, inventory), "The player must have a name.");
        }
    }

    @Nested
    @DisplayName("Positive test cases")
    public class PositiveTestCase {

        @Test
        @DisplayName("Test get name method")
        public void testGetName() {
            assertEquals(player.getName(), name);
        }

        @Test
        @DisplayName("Test add health with positive input")
        public void testAddHealthWithPositiveInput() {
            player.addHealth(6);
            assertEquals(health + 6, player.getHealth());
        }

        @Test
        @DisplayName("Test add health with negative input")
        public void testAddHealthWithNegativeInput() {
            player.addHealth(-4);
            assertEquals(health - 4, player.getHealth());
        }

        @Test
        @DisplayName("Test getHealth method")
        public void testGetHealth() {
            assertEquals(health, player.getHealth());
        }

        @Test
        @DisplayName("Test add score method with positive input")
        public void testAddScoreWithPositiveInput() {
            player.addScore(8);
            assertEquals(score + 8, player.getScore());
        }

        @Test
        @DisplayName("Test add score method with negative input")
        public void testAddScoreWithNegativeInput() {
            player.addScore(-3);
            assertEquals(score - 3, player.getScore());
        }

        @Test
        @DisplayName("Test getScore")
        public void testGetScore() {
            assertEquals(score, player.getScore());
        }

        @Test
        @DisplayName("Test add gold method with positive input")
        public void testAddGoldWithPositiveInput(){
            player.addGold(8);
            assertEquals(gold + 8, player.getGold());
        }

        @Test
        @DisplayName("Test add gold method with negative input")
        public void testAddGoldWithNegativeInput() {
            player.addGold(-8);
            assertEquals(gold - 8, player.getGold());
        }

        @Test
        @DisplayName("Test get gold method")
        public void testGetGold() {
            assertEquals(gold, player.getGold());
        }

        @Test
        @DisplayName("Test addInventory")
        public void testAddInventory() {
            player.addInventory("Knife");
            assertTrue(player.getInventory().contains(("Knife")));
        }

        @Test
        @DisplayName("Test getInventory")
        public void testGetInventory() {
            assertEquals(inventory, player.getInventory());
        }

        @Test
        @DisplayName("Test Player Builder")
        public void testPlayerBuilder() {
            String playerName = "John";
            int playerHealth = 100;
            int playerScore = 50;
            int playerGold = 10;
            List<String> playerInventory = new ArrayList<>();
            playerInventory.add("Sword");
            playerInventory.add("Shield");

            Player.PlayerBuilder builder = new Player.PlayerBuilder();
            Player player = builder
                .name(playerName)
                .health(playerHealth)
                .score(playerScore)
                .gold(playerGold)
                .inventory(playerInventory)
                .build();

            assertEquals(playerName, player.getName());
            assertEquals(playerHealth, player.getHealth());
            assertEquals(playerScore, player.getScore());
            assertEquals(playerGold, player.getGold());
            assertEquals(playerInventory, player.getInventory());
        }

        @Test
        @DisplayName("Test Player Builder with default score and inventory")
        public void testPlayerBuilderWithDefaultScoreAndInventory() {
            String playerName = "Alice";
            int playerHealth = 100;
            int playerGold = 5;

            Player.PlayerBuilder builder = new Player.PlayerBuilder();
            Player player = builder
                .name(playerName)
                .health(playerHealth)
                .gold(playerGold)
                .build();

            assertEquals(playerName, player.getName());
            assertEquals(playerHealth, player.getHealth());
            assertEquals(0, player.getScore());
            assertEquals(playerGold, player.getGold());
            assertNotNull(player.getInventory());
            assertTrue(player.getInventory().isEmpty());
        }
    }

    @Nested
    @DisplayName("Negative test cases")
    public class negativeTestCase {

        @Test
        @DisplayName("If user input is blank when adding an item should throw IllegalArgumentException")
        public void blankUserInputWhenAddingAnItem() {
            assertThrows(IllegalArgumentException.class, () -> player.addInventory(""), "Write an inventory for the player");
        }

        @Test
        @DisplayName("Check if method throws if health is below 0")
        public void ifMethodThrowsWhenHealthIsBelowZero() {
            assertThrows(IllegalArgumentException.class, () -> new Player("name", -4, 0, 0, inventory));
        }

        @Test
        @DisplayName("Throws when item in add inventory method is null")
        public void throwsWhenItemIsNull() {
            String item = null;
            assertThrows(IllegalArgumentException.class, () -> player.addInventory(item));
        }
    }

}


