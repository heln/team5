package edu.ntnu.idatt2001.domain;

import edu.ntnu.idatt2001.Models.*;
import org.junit.jupiter.api.BeforeEach;

import edu.ntnu.idatt2001.Models.goals.Goal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    private List<Goal> goals;
    private Game game;
    private List<String> inventory;
    private List<Link> links;
    private Player player;
    private Passage openingPassage;
    private Story story;
    private Map<Link, Passage> passages;

    @BeforeEach
    public void setUp() {
        passages = new HashMap<>();

        inventory = new ArrayList<>();
        inventory.add("item");
        this.player = new Player("name", 1, 1, 1, inventory);

        links = new ArrayList<>();
        Link link = new Link("text", "reference");
        links.add(link);

        this.openingPassage = new Passage("title", "content", links);
        this.story = new Story("title", openingPassage);

        this.goals = new ArrayList<>();

        this.game = new Game(player, story, goals);
    }

    @Nested
    public class ConstructorTests {

        @Test
        @DisplayName("Test constructor when all values are valid")
        public void testConstructorWithValidValues() {
            assertEquals(player, game.getPlayer());
            assertEquals(story, game.getStory());
            assertEquals(goals, game.getGoals());
        }

        @Test
        @DisplayName("Constructor should throw when Player is null")
        public void testConstructorWhenPlayerIsNull() {
            player = null;
            assertThrows(IllegalArgumentException.class, () -> new Game(player, story, goals));
        }

        @Test
        @DisplayName("Constructor should throw when Story is null")
        public void testConstructorWhenStoryIsNull() {
            story = null;
            assertThrows(IllegalArgumentException.class, () -> new Game(player, story, goals));
        }

        @Test
        @DisplayName("Constructor should throw when Goals is null")
        public void testConstructorWhenGoalsIsNull() {
            goals = null;
            assertThrows(IllegalArgumentException.class, () -> new Game(player, story, goals));
        }
    }

    @Nested
    public class PositiveTestCases {

        @Test
        @DisplayName("Test begin method")
        public void testBeginMethod() {
            assertEquals(openingPassage, game.begin());
        }

        @Test
        @DisplayName("Test go method when inputs are valid")
        public void testGoMethodWithValidInput() {
            Link link = new Link("text", "title");
            Passage passage = new Passage("title", "content", links);
            passages.put(link, passage);

            assertEquals(passage, game.go(link));
        }
    }

    @Nested
    public class NegativeTestCases {

        @Test
        @DisplayName("Test go method when Link is null")
        public void testGoMethodWhenLinkIsNull() {
            Link link = null;
            passages.put(link, openingPassage);

            assertThrows(IllegalArgumentException.class, () -> game.go(link));
        }
    }
}