package edu.ntnu.idatt2001.domain;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


class StoryTest {
  private List<Link> links;
  private Passage openingPassage;
  private Story story;
  private String storyTitle;

  @BeforeEach
  void setUp(){
    Link link1 = new Link("text1", "reference1");
    Link link2 = new Link("text2", "reference2");
    this.links = new ArrayList<>();
    links.add(link1);
    links.add(link2);

    this.openingPassage = new Passage("Title", "content", links);
    this.storyTitle = "title";
    this.story = new Story(storyTitle, openingPassage);
  }

  @Nested
  @DisplayName("Positive test cases")
  public class PositiveTestCase{
    @Test
    @DisplayName("Testing get title method")
    void getTitle() {
      assertEquals(story.getTitle(), storyTitle);
    }

    @Test
    @DisplayName("Testing get opening passage method")
    void getOpeningPassage() {
      assertEquals(story.getOpeningPassage(), openingPassage);
    }

    @Test
    @DisplayName("Testing get passage method")
    void getPassage() {
      Passage passage1 = new Passage("passage 1 title", "testing", links);
      Passage passage2 = new Passage("passage 2 title", "testing testing", links);
      story.addPassage(passage1);
      story.addPassage(passage2);
      Link passage1Link = new Link("text", "passage 1 title");
      assertEquals(passage1, story.getPassage(passage1Link));
    }

    @Test
    @DisplayName("Testing get passages method")
    void getPassages() {
      HashSet<Passage> passages = new HashSet<>();
      passages.add(openingPassage);
      assertEquals(passages, story.getPassages());
    }

    @Test
    @DisplayName("Testing add passage method")
    void addPassage() {
      Passage p = new Passage("Passage title", "content", links);
      story.addPassage(p);
      assertTrue(story.getPassages().contains((p)));
    }

    @Test
    @DisplayName("Remove passage")
    void removePassage() {
      List<Link> links1 = new ArrayList<>();
      Link link1ToP3 = new Link("link1 to p3", "p3");
      links1.add(link1ToP3);

      Passage p1 = new Passage("p1", "content", links1);
      Passage p3 = new Passage("p3", "content", new ArrayList<>());

      Story story1 = new Story("story title", p1);
      story1.addPassage(p3);

      story1.removePassage(link1ToP3);
      assertFalse(story1.getPassages().contains((p3)));
    }

    @Test
    @DisplayName("Get broken links")
    void getBrokenLinks() {
      List<Link> links1 = new ArrayList<>();
      Link brokenLink = new Link("broken link", "nowhere");
      links1.add(brokenLink);

      Passage p1 = new Passage("p1", "content", links1);

      Story story1 = new Story("story title", p1);

      List<Link> brokenLinks = new ArrayList<>();
      brokenLinks.add(brokenLink);
      assertEquals(brokenLinks, story1.getBrokenLinks());
    }
  }

  @Nested
  @DisplayName("Negative test cases")
  public class NegativeTestCase{

    @Test
    @DisplayName("Creating a story object with blank text input should throw IllegalArgumentException")
    void createStoryWithBlankTextInput(){
      assertThrows(IllegalArgumentException.class, () -> new Story("  ", openingPassage), "The passage must have a title.");
    }

    @Test
    @DisplayName("Creating a story object with null openingPassage should throw IllegalArgumentException")
    void createStoryWithNullOpeningPassage(){
      assertThrows(IllegalArgumentException.class, () -> new Story("title", null), "The opening passage does not exist.");
    }

    @Test
    @DisplayName("Adding a passage that is null should throw illegalArgumentException")
    void addNullPassage(){
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(null));
    }

    @Test
    @DisplayName("Getting a passage with a link that is null should throw illegalArgumentException")
    void getPassageWithNullLink(){
      assertThrows(IllegalArgumentException.class, () -> story.getPassage(null));
    }

    @Test
    @DisplayName("Remove passage method when links refer to the same passage should throw IllegalArgumentException")
    void linkConnectedToPassageWithMultipleLinksShouldThrow() {
      List<Link> links1 = new ArrayList<>();
      Link link1ToP3 = new Link("link1 to p3", "p3");
      links1.add(link1ToP3);

      List<Link> links2 = new ArrayList<>();
      Link link2ToP3 = new Link("link2 to p3", "p3");
      links2.add(link2ToP3);

      Passage p1 = new Passage("p1", "content", links1);
      Passage p2 = new Passage("p2", "content", links2);
      Passage p3 = new Passage("p3", "content", new ArrayList<>());

      Story story1 = new Story("story title", p1);
      story1.addPassage(p2);
      story1.addPassage(p3);

      assertThrows(IllegalArgumentException.class, () -> story1.removePassage(link1ToP3));
    }
  }
}