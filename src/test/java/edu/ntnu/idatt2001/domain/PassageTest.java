package edu.ntnu.idatt2001.domain;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PassageTest {
    private String title;
    private String content;
    private List<Link> links;
    private Passage passage;

    @BeforeEach
    public void setUp() {
        links = new ArrayList<>();
        Link link = new Link("text", "reference");
        Link link1 = new Link("text1", "reference1");
        links.add(link);
        links.add(link1);

        this.title = "title";
        this.content = "content";
        this.passage = new Passage("title", "content", links);
    }

    @Nested
    public class ConstructorTests {

        @Test
        @DisplayName("Testing constructor when input is valid")
        public void testConstructorWhenInputIsValid() {
            assertEquals(title, passage.getTitle());
            assertEquals(content, passage.getContent());
            assertEquals(links, passage.getLinks());
        }

        @Test
        @DisplayName("Testing constructor when title is blank")
        public void whenTitleIsBlank() {
            title = "";
            assertThrows(IllegalArgumentException.class, () -> new Passage(title, content, links));
        }

        @Test
        @DisplayName("Testing constructor when content is blank")
        public void throwsWhenContentIsBlank() {
            content = "";
            assertThrows(IllegalArgumentException.class, () -> new Passage(title, content, links));
        }
    }

    @Nested
    public class PositiveTestCases {

        @Test
        @DisplayName("Test add link method when input is valid")
        public void testAddLinkWithValidInput() {
            Link link2 = new Link("text", "reference");
            passage.addLink(link2);

            assertTrue(passage.getLinks().contains(link2));
        }

        @Test
        @DisplayName("Has links method when Link has links should return true")
        public void testHasLinksMethodWithLinks() {
            Link link2 = new Link("text", "reference");
            passage.addLink(link2);

            assertTrue(passage.hasLinks());
        }

        @Test
        @DisplayName("Test equals method when equal")
        public void testEqualsMethodWhenEqual() {
            Passage passage1 = new Passage("title", "content", links);
            assertEquals(passage, passage1);
        }

        @Test
        @DisplayName("Test equals method when unequal")
        public void testEqualsMethodWhenUnequal() {
            Passage passage1 = new Passage("title1", "content1", links);
            assertNotEquals(passage, passage1);
        }
    }

    @Nested
    public class NegativeTestCases {

        @Test
        @DisplayName("Add link method should throw when link is null")
        public void addLinkMethodThrowsWhenLinkIsNull() {
            Link link2 = null;

            assertThrows(IllegalArgumentException.class, () -> passage.addLink(link2));
        }
    }

}