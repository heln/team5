package edu.ntnu.idatt2001.domain.actions;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.actions.InventoryAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InventoryActionTest {
  private Player player;
  private List<String> inventory;

  @BeforeEach
  void generateInformation(){
    inventory = new ArrayList<>();
    inventory.add("item");
    player = new Player("name", 0, 0, 0, inventory);
  }


  @Test
  @DisplayName("execute scoreAction(10) should add 10 to the players health")
  void execute() {
    InventoryAction inventoryAction = new InventoryAction("Sword");
    inventoryAction.execute(player);
    inventory.add("Sword");
    assertEquals(inventory, player.getInventory());
  }


  @Test
  @DisplayName("player null should throw illegalArgumentException")
  void playerNullShouldThrowIllegalArgumentException(){
    InventoryAction inventoryAction = new InventoryAction("Sword");
    Player testPlayer = null;
    assertThrows(IllegalArgumentException.class, () -> inventoryAction.execute(testPlayer), "The player does not exist.");
  }

}