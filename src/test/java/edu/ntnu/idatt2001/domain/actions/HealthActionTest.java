package edu.ntnu.idatt2001.domain.actions;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.actions.HealthAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HealthActionTest {
  private Player player;

  @BeforeEach
  void generateInformation(){
    List<String> inventory = new ArrayList<>();
    inventory.add("item");
    player = new Player("name", 5, 0, 0, inventory);
  }


  @Test
  @DisplayName("execute healthAction(10) should add 10 to the players health")
  void executeAddHealth() {
    HealthAction healthAction = new HealthAction(10);
    healthAction.execute(player);
    assertEquals(15, player.getHealth());
  }

  @Test
  @DisplayName("execute healthAction(-5) should remove 5 from players health")
  void executeRemoveHealth() {
    HealthAction healthAction = new HealthAction(-5);
    healthAction.execute(player);
    assertEquals(0, player.getHealth());
  }


  @Test
  @DisplayName("player null should throw illegalArgumentException")
  void playerNullShouldThrowIllegalArgumentException(){
    HealthAction healthAction = new HealthAction(10);
    Player testPlayer = null;
    assertThrows(IllegalArgumentException.class, () -> healthAction.execute(testPlayer), "The player does not exist.");
  }
}