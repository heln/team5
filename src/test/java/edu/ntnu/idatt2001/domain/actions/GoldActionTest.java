package edu.ntnu.idatt2001.domain.actions;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.actions.GoldAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GoldActionTest {
  private Player player;

  @BeforeEach
  void generateInformation(){
    List<String> inventory = new ArrayList<>();
    inventory.add("item");
    player = new Player("name", 0, 0, 0, inventory);
  }

  @Test
  @DisplayName("execute goldAction should add 10 to the players gold")
  void executeAddGold() {
    GoldAction goldAction = new GoldAction(10);
    goldAction.execute(player);
    assertEquals(10, player.getGold());
  }

  @Test
  @DisplayName("execute healtAction should remove 5 from players health")
  void executeRemoveGold() {
    GoldAction goldAction = new GoldAction(-5);
    goldAction.execute(player);
    assertEquals(-5, player.getGold());
  }

  @Test
  @DisplayName("player null should throw illegalArgumentException")
  void playerNullShouldThrowIllegalArgumentException(){
    GoldAction goldAction = new GoldAction(10);
    Player testPlayer = null;
    assertThrows(IllegalArgumentException.class, () -> goldAction.execute(testPlayer), "The player does not exist.");
  }
}