package edu.ntnu.idatt2001.domain.actions;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.actions.ScoreAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScoreActionTest {
  private Player player;

  @BeforeEach
  void generateInformation(){
    List<String> inventory = new ArrayList<>();
    inventory.add("item");
    player = new Player("name", 0, 5, 0, inventory);
  }


  @Test
  @DisplayName("execute scoreAction(10) should add 10 to the players health")
  void execute() {
    ScoreAction scoreAction = new ScoreAction(10);
    scoreAction.execute(player);
    assertEquals(15, player.getScore());
  }


  @Test
  @DisplayName("player null should throw illegalArgumentException")
  void playerNullShouldThrowIllegalArgumentException(){
    ScoreAction scoreAction = new ScoreAction(10);
    Player testPlayer = null;
    assertThrows(IllegalArgumentException.class, () -> scoreAction.execute(testPlayer), "The player does not exist.");
  }

}