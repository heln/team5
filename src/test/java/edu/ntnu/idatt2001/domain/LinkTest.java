package edu.ntnu.idatt2001.domain;

import edu.ntnu.idatt2001.Models.actions.Action;
import edu.ntnu.idatt2001.Models.actions.ScoreAction;
import edu.ntnu.idatt2001.Models.Link;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LinkTest {
    private String text;
    private String reference;
    private Link link;
    private Action action;

    @BeforeEach
    void generateInformation(){
        this.text = "text";
        this.reference = "reference";
        this.link = new Link(text, reference);
        this.action = new ScoreAction(10);
    }

    @Nested
    @DisplayName("Positive test cases")
    public class PositiveTestCase{
        @Test
        void getText() {
            assertEquals(text, link.getText());
        }

        @Test
        void getReference() {
            assertEquals(reference, link.getReference());
        }

        @Test
        void addActionTest(){
            link.addAction(action);
            assertTrue(link.getActions().contains(action));
        }

        @Test
        void getActions() {
            Action a = new ScoreAction(3);
            link.addAction(action);
            link.addAction(a);

            List<Action> actions = new ArrayList<>();
            actions.add(action);
            actions.add(a);
            assertEquals(actions, link.getActions());
        }

        @Test
        @DisplayName("Testing toString method")
        void testToString() {
            List<Action> actions = new ArrayList<>();
            assertEquals("Link:" + "text='" + text + '\'' +
                ", reference='" + reference + '\'' + ", actions=" + actions, link.toString());
        }

        @Test
        @DisplayName("Testing equals method")
        void testEquals() {
            Link link2 = new Link(text, reference);
            assertEquals(link, link2);
        }
    }

    @Nested
    @DisplayName("Negative test cases")
    public class NegativeTestCase{
        @Test
        @DisplayName("Creating a link with blank text input should throw illegalArgumentException")
        void createLinkWithBlankTextInput(){
            assertThrows(IllegalArgumentException.class, () -> new Link("", "reference"));
        }

        @Test
        @DisplayName("Creating a link with blank reference input should throw illegalArgumentException")
        void createLinkWithBlankReferenceInput(){
            assertThrows(IllegalArgumentException.class, () -> new Link("text", ""));
        }

        @Test
        @DisplayName("Adding an action that is null should throw illegalArgumentException")
        void addNullAction(){
            assertThrows(IllegalArgumentException.class, () -> link.addAction(null));
        }

        @Test
        @DisplayName("Two link objects that doesn't have the same information should not be equal")
        void testNotEquals() {
            Link link2 = new Link("not the same text", "not the same reference");
            assertNotEquals(link, link2);
        }
    }

}