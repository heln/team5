package edu.ntnu.idatt2001.domain.goals;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.InventoryGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InventoryGoalTest {
    private List<String> mandatoryItems = new ArrayList<>();

    @Nested
    public class ConstructorTests {
        @Test
        @DisplayName("Test constructor when values are valid")
        public void constructorTestWhenValuesAreValid() {
            mandatoryItems.add("item");
            InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
            assertTrue(inventoryGoal.getMandatoryItems().contains("item"));
        }

        @Test
        @DisplayName("Should throw if list is empty")
        public void throwsWhenListIsEpmty() {
            assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(mandatoryItems), "List is empty.");
        }
    }

    @Nested
    public class positiveTestCases {
        @Test
        @DisplayName("Should return true if list contains all mandatory items")
        public void returnTrueWhenListContainsMandatoryItems() {
            mandatoryItems.add("item");
            InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
            List<String> items = new ArrayList<>();
            items.add("item");
            assertTrue(inventoryGoal.isFulfilled(new Player("name", 2, 3, 4, items)));
        }

        @Test
        @DisplayName("Should return false if list does not contain all mandatory items")
        public void returnFalseIfListDoesNotContainMandatoryItems() {
            mandatoryItems.add("item");
            InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
            List<String> items = new ArrayList<>();
            items.add("item2");
            assertFalse(inventoryGoal.isFulfilled(new Player("name", 2, 3, 4, items)));
        }
    }

    @Nested
    public class NegativeTestCases {
        @Test
        @DisplayName("When player is null it should throw IllegalArgumentExseption")
        public void throwsWhenGoldIsBelowMinimumGold() {
            mandatoryItems.add("item");
            InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);
            Player player = null;
            assertThrows(IllegalArgumentException.class, () -> inventoryGoal.isFulfilled(player));
        }
    }
}