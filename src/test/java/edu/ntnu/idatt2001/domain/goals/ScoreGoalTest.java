package edu.ntnu.idatt2001.domain.goals;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.ScoreGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ScoreGoalTest {
    @Nested
    public class TestConstructor {
        @Test
        @DisplayName("Test constructor with valid value")
        public void constructorWithValidValue() {
            ScoreGoal scoreGoal = new ScoreGoal(10);
            assertEquals(10, scoreGoal.getMinimumPoints());
        }
    }

    @Nested
    public class PositiveTestCases {
        @Test
        @DisplayName("Check if isFulfilled is true when player's score is above minimum score")
        public void whenScoreIsAboveMinimumScoreShouldReturnTrue() {
            List<String> list = new ArrayList<>();
            list.add("item");
            Player player = new Player("name", 3, 11, 11, list);
            ScoreGoal scoreGoal = new ScoreGoal(10);
            assertTrue(scoreGoal.isFulfilled(player));
        }

        @Test
        @DisplayName("Check if isFulfilled is false when player's score is less than minimum gold")
        public void whenScoreIsLessThanMinimumScoreShouldReturnFalse() {
            List<String> list = new ArrayList<>();
            list.add("item");
            Player player = new Player("name", 3, 3, 5, list);
            ScoreGoal scoreGoal = new ScoreGoal(10);
            assertFalse(scoreGoal.isFulfilled(player));
        }
    }

    @Nested
    public class NegativeTestCases {
        @Test
        @DisplayName("When player is null it should throw IllegalArgumentExseption")
        public void throwsWhenGoldIsBelowMinimumGold() {
            ScoreGoal scoreGoal = new ScoreGoal(10);
            Player player = null;
            assertThrows(IllegalArgumentException.class, () -> scoreGoal.isFulfilled(player));
        }
    }
}