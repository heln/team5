package edu.ntnu.idatt2001.domain.goals;

import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.HealthGoal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HealthGoalTest {

    private HealthGoal healthGoal;

    @Nested
    public class TestConstructor {
        @Test
        @DisplayName("Test constructor with valid value")
        public void constructorWithValidValue() {
            HealthGoal healthGoal = new HealthGoal(10);
            assertEquals(10, healthGoal.getMinimumHealth());
        }
    }

    @Nested
    public class PositiveTestCases {
        @Test
        @DisplayName("Check if isFulfilled is true when player's gold is above minimum gold")
        public void whenGoldIsAboveMinimumGoldShouldReturnTrue() {
            List<String> list = new ArrayList<>();
            list.add("item");
            Player player = new Player("name", 11, 3, 11, list);
            HealthGoal healthGoal = new HealthGoal(10);
            assertTrue(healthGoal.isFulfilled(player));
        }

        @Test
        @DisplayName("Check if isFulfilled is false when player's gold is less than minimum gold")
        public void whenGoldIsLessThanMinimumGoldShouldReturnFalse() {
            List<String> list = new ArrayList<>();
            list.add("item");
            Player player = new Player("name", 3, 3, 11, list);
            HealthGoal healthGoal = new HealthGoal(10);
            assertFalse(healthGoal.isFulfilled(player));
        }
    }

    @Nested
    public class NegativeTestCases {
        @Test
        @DisplayName("When player is null it should throw IllegalArgumentExseption")
        public void throwsWhenGoldIsBelowMinimumGold() {
            HealthGoal healthGoal = new HealthGoal(10);
            Player player = null;
            assertThrows(IllegalArgumentException.class, () -> healthGoal.isFulfilled(player));
        }
    }

}