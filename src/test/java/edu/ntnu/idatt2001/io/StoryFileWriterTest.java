package edu.ntnu.idatt2001.io;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StoryFileWriterTest {

  String fileName;
  Passage openingPassage;
  Passage passage;
  Story story;

  @BeforeEach
  void setUp() {
    fileName = "src/main/resources/stories/Test.paths";

    Link link = new Link("Link", "p1");
    List<Link> linksOpeningPassage = new ArrayList<>();
    linksOpeningPassage.add(link);

    openingPassage = new Passage("Opening passage", "some information", linksOpeningPassage);

    Link link1 = new Link("Link 1", "p2");
    Link link2 = new Link("Link 2", "p3");
    List<Link> links = new ArrayList<>();
    links.add(link1);
    links.add(link2);
    passage = new Passage("p1", "testing", links);

    story = new Story("Story title", openingPassage);
    story.addPassage(passage);
  }

  @Test
  void testCreateFile() throws IOException {
    StoryFileWriter.createFile(fileName);
    File file = new File(fileName);
    assertTrue(file.exists());
  }

  @Test
  void testWriteStory() throws IOException {
    StoryFileWriter.writeStory(fileName, story);

    String expected = """
        Story title

        ::Opening passage
        some information
        [Link](p1)

        ::p1
        testing
        [Link 1](p2)
        [Link 2](p3)
        """;

    assertEquals(expected, readFile(fileName));

  }

  /**
   * Reads content in file and returns it as a string.
   *
   * @param filePath the file path
   * @return content in file as a string
   * @throws IOException IO Exception
   */
  private String readFile(String filePath) throws IOException {
    StringBuilder content = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
      String line;
      while ((line = br.readLine()) != null) {
        content.append(line).append("\n");
      }
    }
    return content.toString();
  }

}