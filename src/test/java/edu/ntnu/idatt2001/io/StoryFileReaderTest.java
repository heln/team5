package edu.ntnu.idatt2001.io;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

class StoryFileReaderTest {
  File file;

  @BeforeEach
  void setUp() {
    file = new File ("src/main/resources/stories/Test-Story.paths");
  }

  @Test
  @DisplayName("test is checkFile() throws IOE if file is empty")
  void testIfCheckFileThrowsIOEIfFileIsEmpty() {
    File emptyFile = new File("src/main/resources/stories/Empty-File.paths");
    assertThrows(IOException.class, () -> StoryFileReader.checkFile(emptyFile));
  }

  @Test
  void testGetTilte() {
    try {
      assertEquals("Haunted House", StoryFileReader.getStoryTitle(file));
    } catch (IOException e) {
      fail("IOException was thrown: " + e.getMessage());
    }
  }

  @Test
  void testGetPassageTitle() {
    String textPassage = "beginnings\n" +
        "You are in a small, dimly lit room. \n" +
        "There is a door in front of you.\n" +
        "[Try to open the door](Another room)\n" +
        "{g, 10}";

    try {
      assertEquals("beginnings", StoryFileReader.getPassageTitle(textPassage));
    } catch (IOException e) {
      fail("IOException was thrown: " + e.getMessage());
    }
  }

  @Test
  void testGetPassageContent() {
    String textPassage = "::Beginnings\n\n\n\n" +
        "You are in a small, dimly lit room. \n" +
        "There is a door in front of you.\n" +
        "[Try to open the door](Another room)\n" +
        "{g, 10}";

    assertEquals("You are in a small, dimly lit room. \n" +
        "There is a door in front of you.\n".trim(), StoryFileReader.getPassageContent(textPassage));
  }

  @Test
  void testGetLinkInfoRefrences() {
    String linkReference1 = "the book of spells";
    String linkReference2 = "beginnings";
    ArrayList<String> links = new ArrayList<>();
    links.add(linkReference1);
    links.add(linkReference2);

    String textPassage = "::Another room\n" +
        "\n" +
        "The door opens to another room. You see a desk with a large, dusty book.\n" +
        "[Open the book](The book of spells)\n" +
        "[Go back](Beginnings)";

    assertEquals(links, StoryFileReader.getLinkOrActionInfo(textPassage, "(", ")"));
  }

  @Test
  void testGetLinks() {
    Link link1 = new Link("open the book", "the book of spells");
    Link link2 = new Link("go back", "beginnings");
    ArrayList<Link> links = new ArrayList<>();
    links.add(link1);
    links.add(link2);

    String textPassage = "::Another room\n" +
        "\n" +
        "The door opens to another room. You see a desk with a large, dusty book.\n" +
        "[Open the book](The book of spells)\n\n" +
        "[Go back](Beginnings)";

    assertEquals(links, StoryFileReader.getLinks(textPassage));
  }

  @Test
  void testGetPassages(){
    Link link1 = new Link("try to open the door", "another room");
    ArrayList<Link> linksPassage1 = new ArrayList<>();
    linksPassage1.add(link1);
    Passage passage1 = new Passage("beginnings",
        "You are in a small, dimly lit room. There is a door in front of you.", linksPassage1);


    Link link2 = new Link("open the book", "the book of spells");
    Link link3 = new Link("go back", "beginnings");
    ArrayList<Link> linksPassage2 = new ArrayList<>();
    linksPassage2.add(link2);
    linksPassage2.add(link3);
    Passage passage2 = new Passage("another room",
        "The door opens to another room. You see a desk with a large, dusty book.", linksPassage2);

    ArrayList<Passage> passages = new ArrayList<>();
    passages.add(passage1);
    passages.add(passage2);

    try {
      assertEquals(passages, StoryFileReader.getPassages(file));
    } catch (IOException e) {
      fail("IOException was thrown: " + e.getMessage());
    }
  }

  @Test
  void testGetPassageWithoutLinks() {
    Passage passage = new Passage("the book of spells",
        "The book is open.", new ArrayList<>());

    String textPassage = "The book of spells\n" +
        "The book is open.";
    try {
      assertEquals(passage, StoryFileReader.getPassage(textPassage));
    } catch (IOException e) {
      fail("IOException was thrown: " + e.getMessage());
    }
  }

  //To forskjellige objekt, så derfor funker ikke testen
  @Test
  void testGetStory() {
    Link link1 = new Link("try to open the door", "another room");
    ArrayList<Link> linksPassage1 = new ArrayList<>();
    linksPassage1.add(link1);
    Passage passage1 = new Passage("beginnings",
        "You are in a small, dimly lit room. There is a door in front of you.", linksPassage1);


    Link link2 = new Link("open the book", "the book of spells");
    Link link3 = new Link("go back", "beginnings");
    ArrayList<Link> linksPassage2 = new ArrayList<>();
    linksPassage2.add(link2);
    linksPassage2.add(link3);
    Passage passage2 = new Passage("another room",
        "The door opens to another room. You see a desk with a large, dusty book.", linksPassage2);

    Story story = new Story("Haunted House", passage1);
    story.addPassage(passage2);


    try {
      assertEquals(story, StoryFileReader.getStory(file));
    } catch (IOException e) {
      fail("IOException was thrown: " + e.getMessage());
    }
  }

}