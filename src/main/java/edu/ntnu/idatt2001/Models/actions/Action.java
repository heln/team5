package edu.ntnu.idatt2001.Models.actions;

import edu.ntnu.idatt2001.Models.Player;

/**
 * An action represents a future change in a players score, health, gold or inventory.
 */
public interface Action {
  void execute(Player player);
}
