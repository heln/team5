package edu.ntnu.idatt2001.Models.actions;

import edu.ntnu.idatt2001.Models.Player;
import java.util.Objects;

/**
 * The GoldAction class changes the Players gold.
 */
public class GoldAction implements Action {
  private final int gold;

  /**
   * The constructor creates a goldAction with the given amount of gold.
   *
   * @param gold gold
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }

  /**
   * The method overrides the action interface method.
   * This.gold is added to the players gold.
   *
   * @param player player
   * @throws IllegalArgumentException throws if the player given is null
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player does not exist.");
    }
    player.addGold(gold);
  }

  /**
   * Checks if an object equals the gold goal object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof GoldAction that)) {
      return false;
    }
    return gold == that.gold;
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(gold);
  }
}
