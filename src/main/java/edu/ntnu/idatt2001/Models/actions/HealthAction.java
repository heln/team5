package edu.ntnu.idatt2001.Models.actions;

import edu.ntnu.idatt2001.Models.Player;
import java.util.Objects;

/**
 * The HealthAction class changes the health of a Player.
 */
public class HealthAction implements Action {
  private final int health;

  /**
   * The constructor creates a health action with the given number of health.
   *
   * @param health health
   */
  public HealthAction(int health) {
    this.health = health;
  }

  /**
   * The method overrides the action interface method.
   * This.health is added to the health of the player.
   *
   * @param player player
   * @throws IllegalArgumentException throws if the player given is null
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player does not exist.");
    }
    player.addHealth(health);
  }

  /**
   * Checks if an object equals the health goal object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof HealthAction that)) {
      return false;
    }
    return health == that.health;
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(health);
  }
}
