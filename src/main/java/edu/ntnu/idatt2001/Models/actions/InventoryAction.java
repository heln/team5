package edu.ntnu.idatt2001.Models.actions;

import edu.ntnu.idatt2001.Models.Player;
import java.util.Objects;

/**
 * The InventoryAction class adds an item to the players inventory.
 */
public class InventoryAction implements Action {
  private final String item;

  /**
   * The constructor creates an inventoryAction
   * that should add the given item to the players inventory.
   *
   * @param item item
   */
  public InventoryAction(String item) {
    this.item = item.strip();
  }

  /**
   * The method overrides the action interface method.
   * This.item is added to the players inventory.
   *
   * @param player player
   * @throws IllegalArgumentException throws if the player given is null
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player does not exist.");
    }
    player.addInventory(item);
  }

  /**
   * Checks if an object equals the inventory goal object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof InventoryAction that)) {
      return false;
    }
    return Objects.equals(item, that.item);
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(item);
  }
}
