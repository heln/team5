package edu.ntnu.idatt2001.Models.actions;

import edu.ntnu.idatt2001.Models.Player;
import java.util.Objects;

/**
 * ScoreAction changes the score of a Player.
 */
public class ScoreAction implements Action {
  private final int points;

  /**
   * The constructor creates a scoreAction with the given points
   * that should be added to the players score.
   *
   * @param points points
   */
  public ScoreAction(int points) {
    this.points = points;
  }

  /**
   * The method changes the score of the player by the given points.
   *
   * @param player player
   * @throws IllegalArgumentException throws if the player given is null
   */
  @Override
  public void execute(Player player) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player does not exist.");
    }
    player.addScore(points);
  }

  /**
   * Checks if an object equals the score goal object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ScoreAction that)) {
      return false;
    }
    return points == that.points;
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(points);
  }
}
