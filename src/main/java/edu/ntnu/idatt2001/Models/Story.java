package edu.ntnu.idatt2001.Models;

import java.util.*;

/**
 * The class has a title, an opening passage and a map of all the passages of the story.
 * The key to each story is a link.
 */
public class Story {
  private final String title;
  private Map<Link, Passage> passages;
  private final Passage openingPassage;

  /**
   * The constructor creates a story-object with the given title and opening passage,
   * and creates a new map of passages.
   * The opening passage is added to the map of passages.
   *
   * @param title          title og the story
   * @param openingPassage opening passage
   * @throws IllegalArgumentException throws if the title given is blank
   */
  public Story(String title, Passage openingPassage) throws IllegalArgumentException {
    if (title.isBlank()) {
      throw new IllegalArgumentException("The passage must have a title.");
    }
    if (openingPassage == null) {
      throw new IllegalArgumentException("The opening passage does not exist.");
    }

    this.title = title;
    this.openingPassage = openingPassage;
    this.passages = new HashMap<>();
    addPassage(openingPassage);
  }

  /**
   * The method gets the title of the story.
   *
   * @return title title
   */
  public String getTitle() {
    return title;
  }

  /**
   * The method gets the opening passage.
   *
   * @return opening passage
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * The method adds the given passage to the story.
   *
   * @param passage passage
   * @throws IllegalArgumentException throws if the passage given is null
   */
  public void addPassage(Passage passage) throws IllegalArgumentException {
    if (passage == null) {
      throw new IllegalArgumentException("The passage attempted added does not exist.");
    }
    Link link = new Link(passage.getTitle(), passage.getTitle());
    passages.put(link, passage);
  }

  /**
   * Removes the passage from the story that the given link links to.
   * A passage cannot be removed from the story if there are other links that links to it.
   *
   * @param link the link
   * @throws IllegalArgumentException if the passage has other links to it
   */
  public void removePassage(Link link) throws IllegalArgumentException {
    if (link == null) {
      throw new IllegalArgumentException("The link does not exist.");
    }
    String reference = link.getReference();

    List<String> sameReferences = passages.values().stream()
        .map(Passage::getLinks)
        .flatMap(Collection::stream)
        .map(Link::getReference)
        .filter(r -> r.equals(reference)).toList();

    if (sameReferences.size() > 1) {
      throw new IllegalArgumentException("The passage cannot be removed because other passages links to it.");
    }

    Link keyLink = new Link(link.getReference(), link.getReference());
    passages.remove(keyLink);
  }

  /**
   * Gets the broken links.
   * A link is broken if it doesn't link to a passage that exists in the story.
   *
   * @return the list of broken links
   */
  public List<Link> getBrokenLinks() {
    //list with all the links from all the passages
    List<String> passageTitles = passages.values().stream()
        .map(Passage::getTitle)
        .toList();

    //checks if the key links matches
    List<Link> brokenLinks = passages.values().stream()
        .map(Passage::getLinks)
        .flatMap(Collection::stream)
        .filter(l -> !passageTitles.contains(l.getReference()))
        .toList();

    return brokenLinks;
  }

  /**
   * The method gets the passage that corresponds to the given link.
   *
   * @param link link
   * @return passage. Returns null if there is no passage that correspond to the link
   * @throws IllegalArgumentException throws if the link given is null
   */
  public Passage getPassage(Link link) throws IllegalArgumentException {
    if (link == null) {
      throw new IllegalArgumentException("The link does not exist.");
    }

    List<Passage> passageMatchingLink = passages.values().stream()
        .filter(p -> link.getReference().equals(p.getTitle()))
        .toList();

    if (passageMatchingLink.size() > 1) {
      throw new IllegalArgumentException("Denne linken linker til flere passasjer. Noe feil har skjedd.");
    } else if (passageMatchingLink.size() == 0) {
      throw new IllegalArgumentException("Denne linken linker ikke til noen passasje. Linken er død.");
    } else {
      return passageMatchingLink.get(0);
    }
  }

  /**
   * The method gets the passages.
   *
   * @return a set of all the different passages
   */
  public Collection<Passage> getPassages() {
    return new HashSet<>(passages.values());
  }

  /**
   * Checks if an object equals the story object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Story story)) {
      return false;
    }
    return title.equalsIgnoreCase(story.title) && passages.equals(story.passages)
        && openingPassage.equals(story.openingPassage);
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, passages, openingPassage);
  }
}
