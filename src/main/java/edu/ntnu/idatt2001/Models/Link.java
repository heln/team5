package edu.ntnu.idatt2001.Models;

import edu.ntnu.idatt2001.Models.actions.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A link connects passages.
 * A link has a text that describes a choice or an action to the user,
 * a reference that uniquely describes identifies a passage(same as the passage title),
 * and a list of actions that make it possible to change the fields of the Player.
 */
public class Link {
  private final String text;
  private final String reference;
  private List<Action> actions;

  /**
   * The constructor creates a link with the given text and reference
   * and creates an empty arraylist of actions.
   *
   * @param text text shown to user
   * @param reference reference to passage
   * @throws IllegalArgumentException throws if text or reference given are blank
   */
  public Link(String text, String reference) throws IllegalArgumentException {
    if (text.isBlank()) {
      throw new IllegalArgumentException("The link must have a text");
    }
    if (reference.isBlank()) {
      throw new IllegalArgumentException("The link must have a reference");
    }
    this.text = text;
    this.reference = reference;
    this.actions = new ArrayList<>();
  }

  /**
   * Gets the text.
   *
   * @return text
   */
  public String getText() {
    return text;
  }

  /**
   * Gets the reference.
   *
   * @return reference
   */
  public String getReference() {
    return reference;
  }

  /**
   * The method adds the given action to the list of actions.
   *
   * @param action action
   */
  public void addAction(Action action) throws IllegalArgumentException {
    if (action == null) {
      throw new IllegalArgumentException("The action does not exist.");
    }
    actions.add(action);
  }

  /**
   * Gets the list of actions.
   *
   * @return actions
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * To string method.
   *
   * @return returns the link as a string
   */
  @Override
  public String toString() {
    return "Link:" + "text='" + text + '\''
        + ", reference='" + reference + '\''
        + ", actions=" + actions;
  }

  /**
   * Checks if an object equals the link object.
   *
   * @param o object
   * @return true if the objects are equal, and false if they aren't
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Link link = (Link) o;
    return Objects.equals(text, link.text) && Objects.equals(reference, link.reference)
        && Objects.equals(actions, link.actions);
  }


  /**
   * Checks if the objects are equal.
   *
   * @return returns either
   */
  @Override
  public int hashCode() {
    return Objects.hash(text, reference, actions);
  }
}
