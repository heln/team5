package edu.ntnu.idatt2001.Models.goals;

import edu.ntnu.idatt2001.Models.Player;

/**
 * Checks if the player has good enough health to
 * get points.
 * Implements method from Goal.
 */
public class HealthGoal implements Goal {
  private final int minimumHealth;

  /**
   * Constructor. Creates an object of the class.
   *
   * @param minimumHealth minimum health in order
   *                      to get points
   */
  public HealthGoal(int minimumHealth) {
    this.minimumHealth = minimumHealth;
  }

  /**
   * Gets minimum health.
   *
   * @return returns minimum health
   */
  public int getMinimumHealth() {
    return minimumHealth;
  }

  /**
   * Checks if the demands are valid, and if
   * the player exists.
   *
   * @param player the chosen player
   * @return returns if the health is good enough in order to get points via "true" or "false"
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("This player does not exist.");
    }

    return player.getHealth() >= minimumHealth;
  }
}
