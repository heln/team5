package edu.ntnu.idatt2001.Models.goals;

import edu.ntnu.idatt2001.Models.Player;

/**
 * The Goal interface represents a goal in a game.
 * Implementations of this interface define specific behavior for achieving the goal.
 */
public interface Goal {

  /**
   * Checks whether the goal is fulfilled for the given player.
   */
  boolean isFulfilled(Player player);
}
