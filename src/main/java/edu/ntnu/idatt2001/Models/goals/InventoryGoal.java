package edu.ntnu.idatt2001.Models.goals;

import edu.ntnu.idatt2001.Models.Player;
import java.util.HashSet;
import java.util.List;

/**
 * Checks if a player has enough
 * mandatory items in order to get points.
 */
public class InventoryGoal implements Goal {
  private final List<String> mandatoryItems;

  /**
   * Constructor. Creates an object of the class.
   *
   * @param mandatoryItems list of mandatory items
   * @throws IllegalArgumentException throws if input is invalid
   */
  public InventoryGoal(List<String> mandatoryItems) throws IllegalArgumentException {
    if (mandatoryItems.isEmpty()) {
      throw new IllegalArgumentException("List has to contain item");
    }

    this.mandatoryItems = mandatoryItems;
  }

  /**
   * Gets the mandatory items in a list.
   *
   * @return returns the items in a list
   */
  public List<String> getMandatoryItems() {
    return mandatoryItems;
  }

  /**
   * Gets the mandatory items as a string.
   *
   * @return returns the items as a string
   */
  public String getMandatoryItemsToString() {
    StringBuilder text = new StringBuilder();
    for (String item : mandatoryItems) {
      text.append(item).append(" ");
    }
    return text.toString();
  }

  /**
   * Checks if a player exists, and
   * if it has enough mandatory items.
   *
   * @param player the chosen player
   * @return returns "true" if the player has the mandatory items, and "false" if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("This player does not exist.");
    }

    return new HashSet<>(player.getInventory()).containsAll(mandatoryItems);
  }
}
