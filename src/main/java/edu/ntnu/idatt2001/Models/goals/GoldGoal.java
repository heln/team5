package edu.ntnu.idatt2001.Models.goals;

import edu.ntnu.idatt2001.Models.Player;

/**
 * Checks if the player has enough gold to
 * get points.
 * Implements method from Goal.
 */
public class GoldGoal implements Goal {
  private final int minimumGold;

  /**
   * Constructor. Creates an object of the class.
   *
   * @param minimumGold the minimum amount of gold
   *                    in order for the points to
   *                    be valid
   */
  public GoldGoal(int minimumGold) {
    this.minimumGold = minimumGold;
  }

  /**
   * Gets minimum gold.
   *
   * @return returns minimum gold
   */
  public int getMinimumGold() {
    return minimumGold;
  }

  /**
   * Checks if the player exists, and
   * if the demands are valid.
   *
   * @param player the chosen player
   * @return returns if the points are valid in form of a boolean
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("This player does not exist.");
    }

    return player.getGold() >= minimumGold;
  }
}
