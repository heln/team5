package edu.ntnu.idatt2001.Models.goals;

import edu.ntnu.idatt2001.Models.Player;

/**
 * Checks if a player has enough points to
 * get valid points.
 */
public class ScoreGoal implements Goal {
  private final int minimumPoints;

  /**
   * Constructor. Creates an object of the class.
   *
   * @param minimumPoints the minimum points the player
   *                      needs to get valid points
   */
  public ScoreGoal(int minimumPoints) {
    this.minimumPoints = minimumPoints;
  }

  /**
   * Gets minimum points.
   *
   * @return returns minimum points
   */
  public int getMinimumPoints() {
    return minimumPoints;
  }

  /**
   * Checks if the demands are fulfuilled,
   * and if the player exists.
   *
   * @param player the chosen player
   * @return returns "true" if the player has enough in
   *         score to get valid points, and "false" if not
   */
  @Override
  public boolean isFulfilled(Player player) {
    if (player == null) {
      throw new IllegalArgumentException("This player does not exist.");
    }

    return player.getScore() > minimumPoints;

  }
}
