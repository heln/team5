package edu.ntnu.idatt2001.Models;

import java.util.List;
import java.util.Objects;

/**
 * Passage class for the game.
 * A passage is a smaller part of the story.
 * One moves from one to another via a link.
 */
public class Passage {
  private final String title;
  private final String content;
  private List<Link> links;

  /**
   * Constructor. Creates a passage object
   * based off of title, content and all the links.
   *
   * @param title   title of the passage
   * @param content content in the passage
   * @param links   list of all the links registered
   * @throws IllegalArgumentException throws if arguments are invalid
   */
  public Passage(String title, String content, List<Link> links) throws IllegalArgumentException {
    checkPassage(title, content);

    this.title = title;
    this.content = content;
    this.links = links;
  }

  /**
   * Checks the user input for faults.
   * Will not create an object if there are
   * flaws.
   *
   * @throws IllegalArgumentException throws if the demands are not valid
   */
  public void checkPassage(String title, String content) throws IllegalArgumentException {
    if (title.isBlank()) {
      throw new IllegalArgumentException("The passage must have a title.");
    }

    if (content.isBlank()) {
      throw new IllegalArgumentException("The passage must have content");
    }
  }

  /**
   * Gets title of the passage.
   *
   * @return returns title of the passage
   */
  public String getTitle() {
    return title;
  }

  /**
   * Gets content in the passage.
   *
   * @return returns content in the passage
   */
  public String getContent() {
    return content;
  }

  /**
   * Adds a link between two passages to a list.
   *
   * @param link the chosen link
   * @return returns "true" if the extension was successful, and "false" if not
   */
  public boolean addLink(Link link) {
    if (link == null) {
      throw new IllegalArgumentException("This link does not exist.");
    }

    links.add(link);
    return true;
  }

  /**
   * Gets the links.
   *
   * @return returns the links.
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * Checks if the list has links.
   *
   * @return returns "true" if there are links in the list
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * Prints out the passage.
   *
   * @return returns the passage printed out
   */
  @Override
  public String toString() {
    return "Passage{" + "title='" + getTitle() + '\'' + ", content='" + getContent() + '\''
        + ", links=" + getLinks() + '}';
  }

  /**
   * Checks if an object equals the passage object.
   *
   * @param o object of choice
   * @return returns boolean based off of the result
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Passage passage = (Passage) o;

    return Objects.equals(title, passage.title) && Objects.equals(content, passage.content)
        && Objects.equals(links, passage.links);
  }

  /**
   * Checks if the objects are equal.
   *
   * @return returns hashcode
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }


}
