package edu.ntnu.idatt2001.Models;

import edu.ntnu.idatt2001.Models.goals.Goal;
import java.util.List;

/**
 * Facade for the game.
 * Connects a player to a story and
 * starts and changes the game.
 *
 * @author sigridronnestad
 * @version 01.03.23
 */
public class Game {
  private Player player;
  private final Story story;
  private List<Goal> goals;

  /**
   * Constructor. Creates an object of the class.
   *
   * @param player player in the game
   * @param story  story of the game
   * @param goals  list of all the goals
   * @throws IllegalArgumentException throws if arguments are invalid
   */
  public Game(Player player, Story story, List<Goal> goals) throws IllegalArgumentException {
    checkGame(player, story, goals);

    this.player = player;
    this.story = story;
    this.goals = goals;
  }

  /**
   * Checks if all values in the game object are valid.
   *
   * @param player the chosen player
   * @param story  the chosen story
   * @param goals  the chosen goals
   * @throws IllegalArgumentException throws if the arguments are invalid
   */
  public void checkGame(Player player, Story story, List<Goal> goals) throws IllegalArgumentException {
    if (player == null) {
      throw new IllegalArgumentException("The player does not exists.");
    }

    if (story == null) {
      throw new IllegalArgumentException("This story does not exist.");
    }

    if (goals == null) {
      throw new IllegalArgumentException("This goal does not exist.");
    }
  }

  /**
   * Gets the player.
   *
   * @return returns the player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Gets the story.
   *
   * @return returns the story
   */
  public Story getStory() {
    return story;
  }

  /**
   * Gets the list of goals.
   *
   * @return returns the list of goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * Starts the game at the first passage.
   *
   * @return returns the first passage
   */
  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * Gets the passage that matches with the given link.
   *
   * @param link chosen link
   * @return returns the matching passage
   * @throws IllegalArgumentException throws if link is invalid
   */
  public Passage go(Link link) throws IllegalArgumentException {
    if (link == null) {
      throw new IllegalArgumentException("This link does not exist.");
    }
    return story.getPassage(link);
  }
}
