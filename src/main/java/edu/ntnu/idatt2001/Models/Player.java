package edu.ntnu.idatt2001.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for the player.
 *
 * @author sigridronnestad
 */
public class Player {
  private final String name;
  private int health;
  private int score;
  private int gold;
  private List<String> inventory;

  /**
   * Constructor. Creates a player object.
   *
   * @param name      name of the player
   * @param health    health of the player
   * @param score     the player's score
   * @param gold      the player's amount of gold
   * @param inventory list of inventory the player has
   */
  public Player(String name, int health, int score, int gold, List<String> inventory) {
    checkPlayer(name, health, inventory);

    this.name = name;
    this.health = health;
    this.score = score;
    this.gold = gold;
    this.inventory = inventory;
  }

  /**
   * Checks if all inputs for
   * the player object are valid before
   * its creation.
   *
   * @param name      name of the player
   * @param health    health of the player
   * @param inventory the inventory to the player
   * @throws IllegalArgumentException throws if inputs are invalid
   */
  public void checkPlayer(String name, int health, List<String> inventory) throws IllegalArgumentException {
    if (name.isBlank()) {
      throw new IllegalArgumentException("The player must have a name.");
    }

    if (health < 0) {
      throw new IllegalArgumentException("Health cannot be below 0.");
    }

    if (inventory.isEmpty()) {
      throw new IllegalArgumentException("The player must have items.");
    }
  }

  /**
   * Constructor. Creates a Player object using PlayerBuilder.
   *
   * @param playerBuilder playerBuilder
   */
  private Player(PlayerBuilder playerBuilder) {
    this.name = playerBuilder.name;
    this.health = playerBuilder.health;
    this.score = playerBuilder.score;
    this.gold = playerBuilder.gold;
    this.inventory = playerBuilder.inventory;
  }

  /**
   * Player builder class.
   */
  public static class PlayerBuilder {
    private String name;
    private int health;
    private int score;
    private int gold;
    private List<String> inventory;


    /**
     * Name player builder.
     *
     * @param name the name
     * @return the player builder
     */
    public PlayerBuilder name(String name) {
      this.name = name;
      return this;
    }

    /**
     * Health player builder.
     *
     * @param health the health
     * @return the player builder
     */
    public PlayerBuilder health(int health) {
      this.health = health;
      return this;
    }

    /**
     * Score player builder.
     *
     * @param score the score
     * @return the player builder
     */
    public PlayerBuilder score(int score) {
      this.score = score;
      return this;
    }

    /**
     * Gold player builder.
     *
     * @param gold the gold
     * @return the player builder
     */
    public PlayerBuilder gold(int gold) {
      this.gold = gold;
      return this;
    }

    /**
     * Inventory player builder.
     *
     * @param inventory the inventory
     * @return the player builder
     */
    public PlayerBuilder inventory(List<String> inventory) {
      this.inventory = inventory;
      return this;
    }

    /**
     * Build player.
     *
     * @return the player
     */
    public Player build() {
      if (score == -1) {
        score = 0;
      }
      if (inventory == null) {
        inventory = new ArrayList<>();
      }

      return new Player(this);
    }
  }

  /**
   * Gets the name of the player.
   *
   * @return returns the name of the player
   */
  public String getName() {
    return name;
  }

  /**
   * Adds health points to the player health.
   *
   * @param health how much health points to add
   * @throws IllegalArgumentException throws if input is invalid
   */
  public void addHealth(int health) throws IllegalArgumentException {
    this.health += health;

    if (this.health < 0) {
      this.health = 0;
    }
  }

  /**
   * Gets the health score of the player.
   *
   * @return returns the health score
   */
  public int getHealth() {
    return health;
  }

  /**
   * Adds points to the player's score.
   *
   * @param points the points to add
   * @throws IllegalArgumentException throws if input is invalid
   */
  public void addScore(int points) throws IllegalArgumentException {
    this.score += points;
  }

  /**
   * Gets the player's score.
   *
   * @return returns the score
   */
  public int getScore() {
    return score;
  }

  /**
   * Adds gold points to the player.
   *
   * @param gold the amount of gold to add
   * @throws IllegalArgumentException throws if input is invalid
   */
  public void addGold(int gold) throws IllegalArgumentException {

    this.gold += gold;
  }

  /**
   * Gets the player's gold amount.
   *
   * @return returns the gold amount
   */
  public int getGold() {
    return gold;
  }

  /**
   * Adds inventory to the player's list.
   *
   * @param item the item to add to the list
   * @throws IllegalArgumentException throws if there is no item
   */
  public void addInventory(String item) throws IllegalArgumentException {
    if (item == null) {
      throw new IllegalArgumentException("Item is not found.");
    } else if (item.isBlank()) {
      throw new IllegalArgumentException("Write an item.");
    } else {
      inventory.add(item);
    }
  }

  /**
   * Gets the list of inventory.
   *
   * @return returns the list
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * Gets list of inventory to string.
   *
   * @return inventory as string
   */
  public String getInventoryToString() {
    StringBuilder text = new StringBuilder();
    for (String s : inventory) {
      text.append(s);
      text.append(" ");
    }
    return text.toString();
  }

}
