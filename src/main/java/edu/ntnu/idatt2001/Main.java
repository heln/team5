package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Controllers.InstructionsController;
import edu.ntnu.idatt2001.Controllers.MainMenuController;
import edu.ntnu.idatt2001.Models.Story;
import edu.ntnu.idatt2001.Views.InstructionsView;
import edu.ntnu.idatt2001.Views.MainMenuView;
import edu.ntnu.idatt2001.Views.PassageView;
import edu.ntnu.idatt2001.io.StoryFileReader;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.print.attribute.standard.MediaSize;

import static javafx.application.Application.launch;

public class Main extends Application{
    public static void main(String [] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Main menu
        MainMenuView mainMenuView = new MainMenuView(primaryStage);

        primaryStage.setScene(mainMenuView.getScene());
        primaryStage.setTitle("Paths");
        primaryStage.show();
    }
}
