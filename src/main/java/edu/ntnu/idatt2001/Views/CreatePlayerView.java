package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.CreatePlayerController;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;

/**
 * The type Create player view.
 */
public class CreatePlayerView {
  private final Stage stage;
  private Scene scene;
  private final CreatePlayerController controller;
  private final MainMenuView mainMenuView;

  private TextField nameTextField;
  private Spinner<Integer> goldSpinner;
  private Spinner<Integer> healthSpinner;
  private Spinner<Integer> goldGoalSpinner;
  private Spinner<Integer> healthGoalSpinner;
  private Spinner<Integer> scoreGoalSpinner;
  private HBox hBoxInventoryGoal;
  private List<String> inputInventoryGoal;


  /**
   * Instantiates a new Create player view.
   *
   * @param stage        the stage
   * @param mainMenuView the main menu view
   */
  public CreatePlayerView(Stage stage, MainMenuView mainMenuView) {
    this.stage = stage;
    this.mainMenuView = mainMenuView;
    controller = new CreatePlayerController(this, stage);

    createScene();
  }


  public TextField getNameTextField() {
    return nameTextField;
  }

  public Spinner<Integer> getGoldSpinner() {
    return goldSpinner;
  }

  public Spinner<Integer> getHealthSpinner() {
    return healthSpinner;
  }

  public Spinner<Integer> getGoldGoalSpinner() {
    return goldGoalSpinner;
  }

  public Spinner<Integer> getHealthGoalSpinner() {
    return healthGoalSpinner;
  }

  public Spinner<Integer> getScoreGoalSpinner() {
    return scoreGoalSpinner;
  }

  /**
   * Gets scene.
   *
   * @return the scene
   */
  public Scene getScene() {
    return scene;
  }

  /**
   * Gets main menu view.
   *
   * @return the main menu view
   */
  public MainMenuView getMainMenuView() {
    return mainMenuView;
  }

  /**
   * Creates a hBox with the title for the view
   * and button to the main menu.
   *
   * @return the hBox with the title and button inside
   */
  private HBox createHBoxTitleAndButton() {
    Label title = new Label("Lag spiller");
    title.getStyleClass().add("title");

    Button mainMenu = new Button("Hovedmeny");
    mainMenu.getStyleClass().add("mainMenuButton");

    mainMenu.setOnAction(e -> {
      try {
        controller.handleMainMenuButton();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });

    HBox hBoxTitleAndButton = new HBox(title, mainMenu);
    hBoxTitleAndButton.getStyleClass().add("hBoxTitleAndButton");
    return hBoxTitleAndButton;
  }

  /**
   * Creates a hBox with an input-field for the player's name.
   *
   * @return the hBox with the input-field inside
   */
  private HBox createHBoxName() {
    Label name = new Label("Navn:");
    name.getStyleClass().add("generalText");

    nameTextField = new TextField();

    HBox hBoxName = new HBox(name, nameTextField);
    hBoxName.getStyleClass().add("generalhBox");
    return hBoxName;
  }

  /**
   * Creates a hBox with the spinner for the chosen gold.
   *
   * @return the hBox with the spinner inside
   */
  private HBox createHBoxGold() {
    Label gold = new Label("Gull:");
    gold.getStyleClass().add("generalText");

    goldSpinner = new Spinner<>(0, 100, 0);
    SpinnerValueFactory.IntegerSpinnerValueFactory intGoldFactory
       = (SpinnerValueFactory.IntegerSpinnerValueFactory) goldSpinner.getValueFactory();

    HBox hBoxGold = new HBox(gold, goldSpinner);
    hBoxGold.getStyleClass().add("generalhBox");

    return hBoxGold;
  }

  /**
   * Creates a hBox with the spinner for the chosen health.
   *
   * @return the hBox with the spinner inside
   */
  private HBox createHBoxHealth() {
    Label health = new Label("Helse:");
    health.getStyleClass().add("generalText");

    healthSpinner = new Spinner<>(1, 10, 1);
    SpinnerValueFactory.IntegerSpinnerValueFactory intHealthFactory
        = (SpinnerValueFactory.IntegerSpinnerValueFactory) healthSpinner.getValueFactory();

    HBox hBoxHealth = new HBox(health, healthSpinner);
    hBoxHealth.getStyleClass().add("generalhBox");
    return hBoxHealth;
  }

  /**
   * Creates a hBox with a message.
   *
   * @return the hBox with the message inside
   */
  private HBox createHBoxMessage() {
    Label message = new Label("Sett mål til spiller");
    message.getStyleClass().add("generalText");

    HBox hBoxMessage = new HBox(message);
    hBoxMessage.getStyleClass().add("generalhBox");
    return hBoxMessage;
  }

  /**
   * Creates a hBox with the spinner for the gold goal.
   *
   * @return the hBox with the spinner inside
   * @throws IllegalArgumentException throws if spinner is empty
   */
  private HBox createHBoxGoldGoal() {
    Label goldGoal = new Label("Gullmål:");
    goldGoal.getStyleClass().add("generalText");

    goldGoalSpinner = new Spinner<>(1, 100, 1);
    SpinnerValueFactory.IntegerSpinnerValueFactory intGoldGoalFactory
        = (SpinnerValueFactory.IntegerSpinnerValueFactory) goldGoalSpinner.getValueFactory();

    HBox hBoxGoldGoal = new HBox(goldGoal, goldGoalSpinner);
    hBoxGoldGoal.getStyleClass().add("generalhBox");
    return hBoxGoldGoal;
  }

  /**
   * Creates a hBox with the spinner for the health goal.
   *
   * @return the hBox with the spinner inside
   * @throws IllegalArgumentException throws if spinner is empty
   */
  private HBox createHBoxHealthGoal() {
    Label healthGoal = new Label("Helsemål:");
    healthGoal.getStyleClass().add("generalText");

    healthGoalSpinner = new Spinner<>(1, 10, 1);
    SpinnerValueFactory.IntegerSpinnerValueFactory intHealthGoalFactory
        = (SpinnerValueFactory.IntegerSpinnerValueFactory) healthGoalSpinner.getValueFactory();

    HBox hBoxHealthGoal = new HBox(healthGoal, healthGoalSpinner);
    hBoxHealthGoal.getStyleClass().add("generalhBox");
    return hBoxHealthGoal;
  }

  /**
   * Creates a hBox with the spinner for the score goal.
   *
   * @return the hBox with the spinner inside
   * @throws IllegalArgumentException throws if spinner is empty
   */
  private HBox createHBoxScoreGoal() throws IllegalArgumentException{
    Label scoreGoal = new Label("Poengmål:");
    scoreGoal.getStyleClass().add("generalText");

    scoreGoalSpinner = new Spinner<>(1, 1000, 1);
    SpinnerValueFactory.IntegerSpinnerValueFactory intScoreGoalFactory
        = (SpinnerValueFactory.IntegerSpinnerValueFactory) scoreGoalSpinner.getValueFactory();

    HBox hBoxScoreGoal = new HBox(scoreGoal, scoreGoalSpinner);
    hBoxScoreGoal.getStyleClass().add("generalhBox");
    return hBoxScoreGoal;
  }

  /**
   * Creates a hBox with the checkboxes for
   * inventory inside.
   *
   * @return the hBox with the checkboxes
   */
  private HBox createHBoxInventoryGoal() {
    Label inventoryGoal = new Label("Inventarmål:");
    inventoryGoal.getStyleClass().add("inventoryText");

    VBox vbox = new VBox();

    CheckBox checkbox1 = new CheckBox();
    checkbox1.setText("Kniv");
    checkbox1.getStyleClass().add("custom-checkbox");
    checkbox1.selectedProperty().addListener((observable, oldValue, newValue) -> controller.handleCheckboxAction(checkbox1, newValue));

    CheckBox checkbox2 = new CheckBox();
    checkbox2.setText("Hårbørste");
    checkbox2.getStyleClass().add("custom-checkbox");
    checkbox2.selectedProperty().addListener((observable, oldValue, newValue) -> controller.handleCheckboxAction(checkbox2, newValue));

    CheckBox checkbox3 = new CheckBox();
    checkbox3.setText("Svømmeføtter");
    checkbox3.getStyleClass().add("custom-checkbox");
    checkbox3.selectedProperty().addListener((observable, oldValue, newValue) -> controller.handleCheckboxAction(checkbox3, newValue));

    CheckBox checkbox4 = new CheckBox();
    checkbox4.setText("Fyrstikker");
    checkbox4.getStyleClass().add("custom-checkbox");
    checkbox4.selectedProperty().addListener((observable, oldValue, newValue) -> controller.handleCheckboxAction(checkbox4, newValue));

    CheckBox checkbox5 = new CheckBox();
    checkbox5.setText("Lommelykt");
    checkbox5.getStyleClass().add("custom-checkbox");
    checkbox5.selectedProperty().addListener((observable, oldValue, newValue) -> controller.handleCheckboxAction(checkbox5, newValue));

    vbox.getChildren().addAll(checkbox1, checkbox2, checkbox3, checkbox4, checkbox5);

    inputInventoryGoal = controller.getInputInventoryGoal();

    hBoxInventoryGoal = new HBox(inventoryGoal, vbox);
    hBoxInventoryGoal.getStyleClass().add("generalhBox");
    return hBoxInventoryGoal;
  }

  /**
   * Creates a hBox with the player button inside.
   * If the input is invalid, the application
   * will communicate information.
   *
   * @return the hBox with the button inside
   */
  private HBox createHBoxCreatePlayerButton() {
    Button button = new Button("Lag spiller");
    button.getStyleClass().add("createPlayerButton");

    button.setOnAction(e -> {
      boolean hasFlaw = false;

      if (nameTextField.getText().isEmpty()) {
        nameTextField.getStyleClass().add("flaw");
        hasFlaw = true;
      } else {
        nameTextField.getStyleClass().remove("flaw");
      }

      if (inputInventoryGoal.isEmpty()) {
        hBoxInventoryGoal.getStyleClass().add("flaw");
        hasFlaw = true;
      } else {
        hBoxInventoryGoal.getStyleClass().remove("flaw");
        hBoxInventoryGoal.getStyleClass().add("generalhBox");
      }

      if (!hasFlaw) {
        try {
          controller.handleCreatePlayerButtonClicked();
        } catch (Exception ex) {
          throw new RuntimeException(ex);
        }
      }
    });

    HBox hBoxCreatePlayerButton = new HBox(button);
    hBoxCreatePlayerButton.getStyleClass().add("hBoxCreatePlayer");
    return hBoxCreatePlayerButton;
  }

  /**
   * Creates a scene with all the hBoxes.
   */
  private void createScene() {
    VBox vBox = new VBox();
    vBox.getChildren().addAll(createHBoxTitleAndButton(), createHBoxName(), createHBoxGold(), createHBoxHealth(),
        createHBoxMessage(), createHBoxGoldGoal(), createHBoxHealthGoal(), createHBoxScoreGoal(),
        createHBoxInventoryGoal(), createHBoxCreatePlayerButton());

    String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/CreatePlayerStyle.css")).toExternalForm();
    scene = new Scene(vBox, 720, 670);
    scene.getStylesheets().add(cssPath);
    vBox.getStyleClass().add("vBox");
  }
}
