package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.MainMenuController;
import edu.ntnu.idatt2001.Models.Story;
import edu.ntnu.idatt2001.io.StoryFileReader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * The type Main menu view.
 */
public class MainMenuView {
  private final Stage stage;
  private Scene scene;
  private final MainMenuController controller;
  private TableView<MainMenuController.FileData> tableView;
  private MainMenuController.FileData selectedFile;
  private Story story;

  /**
   * Instantiates a new Main menu view.
   *
   * @param stage the stage
   * @throws IOException the io exception
   */
  public MainMenuView(Stage stage) throws IOException {
    this.stage = stage;
    controller = new MainMenuController(this, stage);

    createScene();
  }

  /**
   * Gets scene.
   *
   * @return the scene
   */
  public Scene getScene() {
    return scene;
  }

  /**
   * Gets story.
   *
   * @return the story
   */
  public Story getStory() {
    return story;
  }

  /**
   * Gets selected file.
   *
   * @return the selected file
   */
  public MainMenuController.FileData getSelectedFile() {
    return selectedFile;
  }

  /**
   * Gets table view.
   *
   * @return the table view
   */
  public TableView<MainMenuController.FileData> getTableView() {
    return tableView;
  }

  /**
   * Text to direct user in a hBox.
   *
   * @return returns the hBox
   */
  private HBox chooseAFile() {
    Label chooseAFile = new Label("Velg en fil:");
    chooseAFile.getStyleClass().add("chooseAFile");
    return new HBox(chooseAFile);
  }

  /**
   * Creates a tableView for the available files with
   * stories for the application. When a row is clicked on,
   * the chosen file will create a story. If the row is
   * double-clicked, the user will be directed to another
   * scene.
   *
   * @return returns the hBox with the table in it
   */
  private HBox createTableView() throws IOException {
    tableView = new TableView<>();
    tableView.getStyleClass().add("tableView");

    TableColumn<MainMenuController.FileData, String> column1 = new TableColumn<>("Filnavn");
    column1.setCellValueFactory(new PropertyValueFactory<>("fileName"));

    TableColumn<MainMenuController.FileData, String> column2 = new TableColumn<>("Lokasjon");
    column2.setCellValueFactory(new PropertyValueFactory<>("location"));

    TableColumn<MainMenuController.FileData, String> column3 = new TableColumn<>("Døde linker");
    column3.setCellValueFactory(new PropertyValueFactory<>("brokenLinks"));

    tableView.getColumns().addAll(column1, column2, column3);
    tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    controller.setUpTable();

    tableView.setOnMouseClicked(e -> {
        if (e.getClickCount() == 2) {
          selectedFile = tableView.getSelectionModel().getSelectedItem();
          File file = selectedFile.getFile();

          try {
            story = StoryFileReader.getStory(file);
            controller.handleCreatePlayerButtonClick();
          } catch (Exception ex) {
            System.out.println(ex.getMessage());
          }

        } else if (e.getClickCount() == 1){
          selectedFile = tableView.getSelectionModel().getSelectedItem();
          File file = selectedFile.getFile();

          try {
            story = StoryFileReader.getStory(file);
          } catch (IOException ex) {
            throw new RuntimeException(ex);
          }
        }
    });

    HBox hBoxTableView = new HBox(tableView);
    hBoxTableView.getStyleClass().add("hBoxTableView");
    return hBoxTableView;
  }

  /**
   * Generates the uploadFile button.
   * When pressed, the chosen file from the computer
   * will be added to the tableView.
   *
   * @return returns the hBow with the button in it
   */
  private HBox createUploadFileButton() {
    Button uploadFileButton = new Button("Last opp egen fil");
    uploadFileButton.getStyleClass().add("uploadFileButton");

    FileChooser fileChooser = new FileChooser();

    uploadFileButton.setOnAction(e -> {
      try {
        controller.handleUploadFileButtonClick(fileChooser.showOpenDialog(stage));
      } catch (IOException ex) {
        if (ex.getMessage().equals("Unsupported file format. Only .paths and .txt-files are supported.")) {
          showAlertBox(ex.getMessage(), "Ugyldig filformat");
          throw new RuntimeException(ex);
        }
        if (ex.getMessage().equals("The file is empty.")) {
          showAlertBox(ex.getMessage(), "Tom fil");
          throw new RuntimeException(ex);
        }
      }
    });

    HBox hBoxUploadFileButtin = new HBox(uploadFileButton);
    uploadFileButton.getStyleClass().add("hBoxUploadFileButton");
    return hBoxUploadFileButtin;
  }

  /**
   * Shows alertbox with the given error message and title.
   *
   * @param errorMessage the error message
   * @param title the title
   */
  private void showAlertBox(String errorMessage, String title) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle(title);
    alert.setContentText(errorMessage + "\n"
        + "Prøv å last opp en ny eller velg en av de forhåndslagde i tabellen.");
    alert.show();
  }

  /**
   * Generates the createPlayer button, which will
   * direct the user to another scene.
   * If the button is pressed without choosing a file first,
   * an alert box will show.
   *
   * @return returns the hBox with the button in it
   */
  private HBox createCreatePlayerButton() {
    Button createPlayerButton = new Button("Lag spiller");
    createPlayerButton.getStyleClass().add("createPlayerButton");

    createPlayerButton.setOnAction(e -> {
      if (story == null) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fil ikke valgt");
        alert.setContentText("Vennligst velg en fil fra tabellen.");
        alert.show();
      } else {
        try {
          controller.handleCreatePlayerButtonClick();
        } catch (Exception ex) {
          throw new RuntimeException(ex);
        }
      }
    });

    HBox hBoxCreatePlayerButton = new HBox(createPlayerButton);
    hBoxCreatePlayerButton.getStyleClass().add("hBoxCreatePlayerButton");
    return hBoxCreatePlayerButton;
  }

  /**
   * Generates the settings button which will direct
   * the user to another scene.
   *
   * @return returns the hBox with the button in it
   */
  private HBox createHeader() {
    Label title = new Label("Historiespill");
    title.getStyleClass().add("title");

    Button settingsButton = new Button("Brukerveiledning");
    settingsButton.getStyleClass().add("settingsButton");
    settingsButton.setOnAction(e -> {
      try {
        controller.handleSettingsButtonClick();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });

    HBox hBoxSettingsButton = new HBox(title, settingsButton);
    hBoxSettingsButton.getStyleClass().add("hBoxSettingsButton");

    return hBoxSettingsButton;
  }

  /**
   * Creates the MainMenu scene with all the elements.
   */
  private void createScene() throws IOException {
    VBox vBox = new VBox();
    vBox.getChildren().addAll(createHeader(), chooseAFile(), createTableView(), createUploadFileButton(), createCreatePlayerButton());
    scene = new Scene(vBox, 720, 670);

    String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/MainMenuStyle.css")).toExternalForm();
    scene.getStylesheets().add(cssPath);
    vBox.getStyleClass().add("vBox");
  }
}
