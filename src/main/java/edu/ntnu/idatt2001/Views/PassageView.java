package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.PassageController;
import edu.ntnu.idatt2001.Models.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * This class represents the user interface of the Passage scene.
 */
public class PassageView {
    private final PassageController controller;
    private final Stage stage;
    private Scene scene;
    private final MainMenuView mainMenuView;
    private Label title;
    private Label content;
    private Dialog<String> dialog;
    private Player player;
    private final HBox hBoxButtons;
    private Label goldValue;
    private Label healthValue;
    private Label scoreValue;
    private Label inventoryValue;
    private final int startGold;
    private final int startHealth;
    private Label message;
    private Game game;


    /**
     * Instantiates a new Passage view.
     *
     * @param stage         the stage
     * @param mainMenuView  the main menu view
     * @param game          the game
     */
    public PassageView(Stage stage, MainMenuView mainMenuView, Game game) {
        this.stage = stage;
        this.mainMenuView = mainMenuView;
        this.game = game;
        player = game.getPlayer();
        startGold = player.getGold();
        startHealth = player.getHealth();

        hBoxButtons = new HBox();

        controller = new PassageController(this, stage);
        createScene();
    }

    /**
     * Gets scene.
     *
     * @return the scene
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public Label getMessage() {
        return message;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Label getTitle() {
        return title;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public Label getContent() {
        return content;
    }

    /**
     * Gets main menu view.
     *
     * @return the main menu view
     */
    public MainMenuView getMainMenuView() {
        return mainMenuView;
    }

    /**
     * Gets game.
     *
     * @return the game
     */
    public Game getGame() {
        return game;
    }

    /**
     * Gets dialog.
     *
     * @return the dialog
     */
    public Dialog<String> getDialog() {
        return dialog;
    }

    /**
     * Gets gold value.
     *
     * @return the gold value
     */
    public Label getGoldValue() {
        return goldValue;
    }

    /**
     * Gets health value.
     *
     * @return the health value
     */
    public Label getHealthValue() {
        return healthValue;
    }

    /**
     * Gets score value.
     *
     * @return the score value
     */
    public Label getScoreValue() {
        return scoreValue;
    }

    /**
     * Gets inventory value.
     *
     * @return the inventory value
     */
    public Label getInventoryValue() {
        return inventoryValue;
    }

    /**
     * Creates main menu button.
     *
     * @return the main menu button
     */
    private Button createMainMenuButton() {
        Button mainMenu = new Button("Hovedmeny");
        mainMenu.getStyleClass().add("mainMenuButton");
        mainMenu.setOnAction(e -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Tilbake til hovedmenyen");
            alert.setContentText("Er du sikker på at du vil tilbake til hovedmyen? \n" +
                "Da blir spilleren og historien slettet.");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                controller.handleMainMenuButton();
            }
        });
        return mainMenu;
    }

    /**
     * Creates restart button.
     *
     * @return the restart button
     */
    private Button createRestartButton() {
        Button restart = new Button("Start på nytt");
        restart.getStyleClass().add("restartButton");
        restart.setOnAction(event -> {
            createButtonsForPassage(game.begin());
            controller.handleRestartButton();
        });
        return restart;
    }

    /**
     * Creates dialog with information about how to play the game.
     */
    private void createDialog() {
        dialog = new Dialog<>();
        dialog.setTitle("Brukerveiledning");
        ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        String text = """
                Les innholdet i passasjen.
                Trykk deretter på knappen
                med ønsket alternativ.
                Deretter kommer du videre til
                neste passasje helt til spillet er over.""";
        dialog.setContentText(text);
        dialog.getDialogPane().getButtonTypes().add(type);
    }

    /**
     * Creates how to button.
     *
     * @return the how to button
     */
    private Button createHowToButton() {
        Button howTo = new Button("Brukerveiledning");
        howTo.getStyleClass().add("howToButton");
        howTo.setOnAction(e -> {
            createDialog();
            controller.handleHowToButton();
        });
        return howTo;
    }

    /**
     * Creates menu. A HBox with the main menu, restart and how-to buttons.
     *
     * @return the menu
     */
    private HBox createMenu() {
        Button mainMenu = createMainMenuButton();
        Button restart = createRestartButton();
        Button howTo = createHowToButton();

        HBox hBox = new HBox();
        hBox.getChildren().addAll(mainMenu, restart, howTo);
        hBox.getStyleClass().add("hBoxButtons");
        return hBox;
    }


    /**
     * Reset player to how it was at the start of the game by making a new player object.
     *
     * @return the player
     */
    public Player resetPlayer() {
        this.player = new Player.PlayerBuilder()
            .name(player.getName())
            .gold(startGold)
            .health(startHealth)
            .score(0)            //TODO endre builder start-score
            .inventory(null)     //TODO endre builder start-inventory
            .build();
        return player;
    }

    /**
     * Creates player info.
     *
     * @return the player info
     */
    private HBox createPlayerInfo() {
        Label health = new Label("Helse: ");
        healthValue = new Label(Integer.toString(player.getHealth()));
        HBox hBoxHealth = new HBox(health, healthValue);
        hBoxHealth.getStyleClass().add("generalhBox");

        Label gold = new Label("Gull: ");
        goldValue = new Label(Integer.toString(player.getGold()));
        HBox hBoxGold = new HBox(gold, goldValue);
        hBoxGold.getStyleClass().add("generalhBox");

        Label score = new Label("Poeng: ");
        scoreValue = new Label(Integer.toString(player.getScore()));
        HBox hBoxScore = new HBox(score, scoreValue);
        hBoxScore.getStyleClass().add("generalhBox");

        Label inventory = new Label("Inventar: ");
        inventoryValue = new Label(player.getInventoryToString());
        HBox hBoxInventory= new HBox(inventory, inventoryValue);
        hBoxInventory.getStyleClass().add("generalhBox");

        List<Label> labels = new ArrayList<>();
        labels.add(health);
        labels.add(healthValue);
        labels.add(gold);
        labels.add(goldValue);
        labels.add(score);
        labels.add(scoreValue);
        labels.add(inventory);
        labels.add(inventoryValue);

        for (Label label : labels) {
            label.getStyleClass().add("generalText");
        }
        HBox hBoxPlayerInfo = new HBox(hBoxHealth, hBoxGold, hBoxScore, hBoxInventory);
        hBoxPlayerInfo.getStyleClass().add("hBoxPlayerInfo");
        return hBoxPlayerInfo;
    }

    /**
     * Creates passage title.
     *
     * @return the title
     */
    private HBox createTitle() {
        title = new Label(firstLetterToUppercase(game.begin().getTitle()));
        title.getStyleClass().add("title");
        HBox hBoxTitle = new HBox(title);
        hBoxTitle.getStyleClass().add("hBoxTitle");
        return hBoxTitle;
    }

    /**
     * First letter to uppercase string.
     *
     * @param string the string
     * @return the string
     */
    public String firstLetterToUppercase(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    /**
     * Creates passage and message content.
     *
     * @return the passage and message content
     */
    private HBox createContentAndMessage() {
        content = new Label(game.begin().getContent());
        content.setWrapText(true);
        content.getStyleClass().add("content");

        message = new Label();
        message.setWrapText(true);
        message.getStyleClass().add("message");

        VBox vBoxMessage = new VBox(content, message);
        vBoxMessage.getStyleClass().add("vBoxMessage");

        HBox hBoxContent = new HBox(vBoxMessage);
        hBoxContent.getStyleClass().add("hBoxContent");
        return hBoxContent;
    }

    /**
     * Creates buttons for each link of a passage.
     *
     * @param passage the passage
     * @return the buttons
     */
    private HBox createButtonsForPassage(Passage passage) {
        hBoxButtons.getChildren().clear();
        message.setText("");
        List<Link> links = passage.getLinks();

        if (links.isEmpty()) {
            Button endButton = new Button("Spillet er ferdig");
            endButton.getStyleClass().add("newButton");
            endButton.setOnAction(e -> controller.handleEndPassage());
            hBoxButtons.getChildren().add(endButton);
        }

        for (Link link : links) {
            Button button = new Button(link.getText());
            button.getStyleClass().add("newButton");
            button.setOnAction(event -> {
                try {
                    createButtonsForPassage(game.go(link));
                    controller.handleLinkButton(link, player);
                } catch (IllegalArgumentException iae) {
                    controller.showAlertBox(iae);
                    button.setOnAction(e -> controller.showAlertBox(iae));
                }
            });
            hBoxButtons.getChildren().add(button);
        }
        hBoxButtons.getStyleClass().add("hBoxNewButtons");
        return hBoxButtons;
    }

    /**
     * Creates the scene with all the elements in the hBoxes.
     */
    public void createScene() {
        VBox vBox = new VBox();
        vBox.getChildren().addAll(createMenu(), createPlayerInfo(),
            createTitle(), createContentAndMessage(), createButtonsForPassage(game.begin()));
        scene = new Scene(vBox, 720, 670);

        String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/PassageStyle.css")).toExternalForm();
        scene.getStylesheets().add(cssPath);
        vBox.getStyleClass().add("vBox");
    }
}
