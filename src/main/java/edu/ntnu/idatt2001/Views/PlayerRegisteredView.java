package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.PlayerRegisteredController;
import edu.ntnu.idatt2001.Models.Game;
import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.*;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Overview of the registered player
 */
public class PlayerRegisteredView {
  private final Stage stage;
  private Scene scene;
  private final PlayerRegisteredController controller;
  private final MainMenuView mainMenuView;
  private Game game;
  private Player player;


  /**
   * Instantiates a new Player registered view.
   *
   * @param stage                  the stage
   * @param game                   the game
   * @param mainMenuView           the main menu view
   */
  public PlayerRegisteredView(Stage stage, Game game, MainMenuView mainMenuView) {
    this.stage = stage;
    this.mainMenuView = mainMenuView;
    this.game = game;
    player = game.getPlayer();
    controller = new PlayerRegisteredController(this, stage);

    createScene();
  }

  /**
   * Gets scene.
   *
   * @return the scene
   */
  public Scene getScene() {
    return scene;
  }

  /**
   * Gets main menu view.
   *
   * @return the main menu view
   */
  public MainMenuView getMainMenuView() {
    return mainMenuView;
  }

  /**
   * Gets game.
   *
   * @return the game
   */
  public Game getGame() {
    return game;
  }

  /**
   * Creates a hBox with the title of the view.
   *
   * @return the hBox with the title
   */
  private HBox createTitle() {
    Label title = new Label("Spiller registrert!");
    title.getStyleClass().add("title");
    HBox hBoxTitle = new HBox(title);
    hBoxTitle.getStyleClass().add("hBoxTitle");
    return hBoxTitle;
  }

  /**
   * Creates hBox with the information
   * about the registered player.
   *
   * @return the hBox with the information
   */
  private HBox createPlayerInfo() {
    StringBuilder info = new StringBuilder("Navn: " + player.getName() + "\nGull: " + player.getGold() +
        "\nHelse: " + player.getHealth() + "\n-------------\n");
    for (Goal goal : game.getGoals()) {
      String goalInfo;
      switch (goal.getClass().getSimpleName()) {
        case "ScoreGoal" -> goalInfo = "Poengmål: " + ((ScoreGoal) goal).getMinimumPoints() + "\n";
        case "HealthGoal" -> goalInfo = "Helsemål: " + ((HealthGoal) goal).getMinimumHealth() + "\n";
        case "GoldGoal" -> goalInfo = "Gullmål: " + ((GoldGoal) goal).getMinimumGold() + "\n";
        case "InventoryGoal" -> goalInfo = "Inventarmål: " + ((InventoryGoal) goal).getMandatoryItemsToString() + "\n";

        default -> throw new IllegalArgumentException("Unexpected goal type: " + goal.getClass().getSimpleName());
      }
      info.append(goalInfo);
    }

    Label text = new Label(info.toString());
    text.setWrapText(true);
    text.getStyleClass().add("playerInfo");

    HBox hBoxPlayerInfo = new HBox(text);
    hBoxPlayerInfo.getStyleClass().add("hBoxPlayerInfo");
    return hBoxPlayerInfo;
  }

  /**
   * Creates hBox with buttons for the main menu
   * and to start the game.
   *
   * @return the hBox with the buttons
   */
  private HBox createButtons() {
    Button mainMenu = new Button("Tilbake til hovedmenyen");
    mainMenu.getStyleClass().add("mainMenuButton");

    Button startGame = new Button("Start spillet");
    startGame.getStyleClass().add("startGameButton");

    mainMenu.setOnAction(e -> {
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
      alert.setTitle("Tilbake til hovedmenyen");
      alert.setContentText("Er du sikker på at du vil tilbake" +
              "til hovedmyen? Da blir spilleren slettet.");

      Optional<ButtonType> result = alert.showAndWait();
      if (result.isPresent() && result.get() == ButtonType.OK) {
        try {
          controller.handleMainMenuButtonClicked();
        } catch (IOException ex) {
          throw new RuntimeException(ex);
        }
      }
    });

    startGame.setOnAction(e -> controller.handleStartGameButtonClicked());

    HBox hBoxButtons = new HBox(mainMenu, startGame);
    hBoxButtons.getStyleClass().add("hBoxButtons");
    return hBoxButtons;
  }

  /**
   * Creates scene with all the hBoxes.
   */
  private void createScene(){
    VBox vBox = new VBox();
    vBox.getChildren().addAll(createTitle(), createPlayerInfo(), createButtons());
    scene = new Scene(vBox, 720, 670);

    String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/PlayerRegisteredStyle.css")).toExternalForm();
    scene.getStylesheets().add(cssPath);
    vBox.getStyleClass().add("vBox");
  }
}
