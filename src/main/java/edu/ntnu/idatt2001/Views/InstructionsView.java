package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.InstructionsController;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

/**
 * Class for the view with the application instructions.
 */
public class InstructionsView {
    private final Stage stage;
    private Scene scene;
    private final InstructionsController controller;
    private final MainMenuView mainMenuView;

    /**
     * Instantiates a new Instructions view.
     *
     * @param stage        the stage
     * @param mainMenuView the main menu view
     * @throws FileNotFoundException the file not found exception
     */
    public InstructionsView(Stage stage, MainMenuView mainMenuView) throws IOException {
        this.stage = stage;
        this.mainMenuView = mainMenuView;
        controller = new InstructionsController(this, stage);

        createScene();
    }

    /**
     * Gets scene.
     *
     * @return the scene
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Gets main menu view.
     *
     * @return the main menu view
     */
    public MainMenuView getMainMenuView() {
        return mainMenuView;
    }

    /**
     * Creates a hBox with the title of the view.
     *
     * @return returns the hBox with the title
     */
    private HBox title() {
        Label title = new Label("Brukerveiledning");
        title.getStyleClass().add("title");
        HBox hBoxTitle = new HBox(title);
        hBoxTitle.getStyleClass().add("hBoxTitle");
        return hBoxTitle;
    }

    /**
     * Creates a hBox with the instructions
     * read from an external file.
     *
     * @return the hBox with the instructions
     * @throws FileNotFoundException the file not found exception
     */
    public HBox createHBoxTextField() throws IOException {
        File file = new File("src/main/resources/Instructions/Brukerveiledning.txt");

        Label label = new Label(controller.writeInstructions(file));
        label.setWrapText(true);
        label.getStyleClass().add("instructions");

        HBox hBoxInstructions = new HBox(label);
        hBoxInstructions.getStyleClass().add("hBoxInstructions");
        return hBoxInstructions;
    }

    /**
     * Creates a hBox with a button leading
     * to the main menu.
     *
     * @return the hBox with the button in it
     */
    private HBox createMainMenuButton() {
        Button mainMenuButton = new Button("Hovedmeny");
        mainMenuButton.getStyleClass().add("mainMenuButton");

        mainMenuButton.setOnAction(e -> {
            try {
                controller.handleMainMenuButtonClicked();
            } catch (FileNotFoundException ex) {
                throw new RuntimeException(ex);
            }
        });

        HBox hBoxMainMenuButton = new HBox(mainMenuButton);
        hBoxMainMenuButton.getStyleClass().add("hBoxMainMenuButton");
        return hBoxMainMenuButton;
    }

    /**
     * Creates a scene with the hBoxes.
     *
     * @throws FileNotFoundException the file not found exception
     */
    public void createScene() throws IOException {
        VBox vBox = new VBox();
        vBox.getChildren().addAll(title(), createHBoxTextField(), createMainMenuButton());
        scene = new Scene(vBox, 720, 670);

        String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/InstructionsStyle.css")).toExternalForm();
        scene.getStylesheets().add(cssPath);
        vBox.getStyleClass().add("vBox");
    }
}
