package edu.ntnu.idatt2001.Views;

import edu.ntnu.idatt2001.Controllers.EndController;
import edu.ntnu.idatt2001.Models.Game;
import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class represent the user interface of the End scene.
 */
public class EndView {
  private final Stage stage;
  private final MainMenuView mainMenuView;
  private final EndController controller;
  private Scene scene;
  private final Player player;
  private Game game;


  /**
   * Instantiates a new End view.
   *
   * @param stage          the stage
   * @param mainMenuView   the main menu view
   * @param game           the game
   */
  public EndView(Stage stage, MainMenuView mainMenuView, Game game) {
    this.stage = stage;
    this.mainMenuView = mainMenuView;
    this.game = game;
    this.player = game.getPlayer();
    controller = new EndController(this, stage);

    createScene();
  }

  /**
   * Gets scene.
   *
   * @return the scene
   */
  public Scene getScene() {
    return scene;
  }

  /**
   * Gets main menu view.
   *
   * @return the main menu view
   */
  public MainMenuView getMainMenuView() {
    return mainMenuView;
  }

  /**
   * Creates the scene title and adds it to a HBox.
   *
   * @return the HBox with the title
   */
  private HBox createTitle() {
    Label title = new Label("Gratulerer! Du har nådd slutten av spillet!");
    title.setWrapText(true);
    title.getStyleClass().add("title");
    HBox hBoxTitle = new HBox(title);
    hBoxTitle.getStyleClass().add("hBoxTitle");
    return hBoxTitle;
  }

  /**
   * Creates the main menu button and adds it to a HBox.
   *
   * @return the HBox with the button
   */
  private HBox createButton() {
    Button mainMenu = new Button("Tilbake til hovedmenyen");
    mainMenu.getStyleClass().add("mainMenuButton");
    mainMenu.setOnAction(e -> controller.handleMainMenuButton());
    HBox hBoxMainMenuButton = new HBox(mainMenu);
    hBoxMainMenuButton.getStyleClass().add("hBoxMainMenuButton");
    return hBoxMainMenuButton;
  }

  /**
   * Creates the headings and adds them to a HBox.
   *
   * @return the HBox with the headings
   */
  private HBox createHeadings() {
    Label player = new Label("Spiller");
    Label goal = new Label("Mål");
    player.getStyleClass().add("heading");
    goal.getStyleClass().add("heading");

    HBox hBoxHeadings = new HBox(player, goal);
    hBoxHeadings.getStyleClass().add("hBoxHeadings");
    return hBoxHeadings;
  }

  /**
   * Sets the text of the goal achieved label to "Mål oppnådd :)" if the given goal is achieved.
   *
   * @param goal the goal
   * @param achievedLabel the goal achieved label
   */
  private void setGoalAchievedText(Goal goal, Label achievedLabel) {
    if (goal.isFulfilled(player)) {
      achievedLabel.setText("Mål oppnådd :)");
    }
  }

  /**
   * Add stylesheets to the labels.
   *
   * @param one Label one
   * @param two Label two
   * @param three Label three
   * @param four Label four
   */
  public void addStyleSheetToLabel(Label one,Label two, Label three, Label four) {
    List<Label> labels = new ArrayList<>();
    labels.add(one);
    labels.add(two);
    labels.add(three);
    labels.add(four);

    for (Label label : labels) {
      label.getStyleClass().add("generalText");
      label.setWrapText(true);
    }
  }

  /**
   * Creates the gold info and adds it to a HBox.
   *
   * @return the HBox with the gold info
   */
  private HBox createGoldInfo() {
    Label gold = new Label("Gull:");
    Label goldValue = new Label(Integer.toString(player.getGold()));
    GoldGoal goal = (GoldGoal) getGoal("GoldGoal");
    Label goldGoal = new Label(Integer.toString(goal.getMinimumGold()));
    Label goldGoalAchieved = new Label();
    setGoalAchievedText(goal, goldGoalAchieved);

    addStyleSheetToLabel(gold, goldValue, goldGoal, goldGoalAchieved);

    HBox hBoxGold = new HBox(gold, goldValue, goldGoal, goldGoalAchieved);
    hBoxGold.getStyleClass().add("hBoxGold");
    return hBoxGold;
  }

  /**
   * Creates the health info and adds it to a HBox.
   *
   * @return the HBox with the health info
   */
  private HBox createHealthInfo() {
    Label health = new Label("Health:");
    Label healthValue = new Label(Integer.toString(player.getHealth()));
    HealthGoal goal = (HealthGoal) getGoal("HealthGoal");
    Label healthGoal = new Label(Integer.toString(goal.getMinimumHealth()));
    Label healthGoalAchieved = new Label();
    setGoalAchievedText(goal, healthGoalAchieved);

    addStyleSheetToLabel(health, healthValue, healthGoal, healthGoalAchieved);

    HBox hBoxHealth = new HBox(health, healthValue, healthGoal, healthGoalAchieved);
    hBoxHealth.getStyleClass().add("hBoxHealth");
    return hBoxHealth;
  }

  /**
   * Gets the goal of the given type from the goals list.
   *
   * @param goalType the goal type
   * @return the goal
   * @throws IllegalArgumentException if the goal type input is incorrect
   */
  private Goal getGoal(String goalType) throws IllegalArgumentException{
    for (Goal goal : game.getGoals()) {
      if (goal.getClass().getSimpleName().equalsIgnoreCase(goalType)) {
        return goal;
      }
    }
    throw new IllegalArgumentException("Must enter a goal type that exists. " +
        "Either GoldGoal, HealthGoal, ScoreGoal or InventoryGoal!");
  }

  /**
   * Creates the score info and adds it to a HBox.
   *
   * @return the HBox with the score info
   */
  private HBox createScoreInfo() {
    Label score = new Label("Poeng:");
    Label scoreValue = new Label(Integer.toString(player.getScore()));
    ScoreGoal goal = (ScoreGoal) getGoal("ScoreGoal");
    Label scoreGoal = new Label(Integer.toString(goal.getMinimumPoints()));
    Label scoreGoalAchieved = new Label();
    setGoalAchievedText(goal, scoreGoalAchieved);

    addStyleSheetToLabel(score, scoreValue, scoreGoal, scoreGoalAchieved);

    HBox hBoxScore = new HBox(score, scoreValue, scoreGoal, scoreGoalAchieved);
    hBoxScore.getStyleClass().add("hBoxScore");
    return hBoxScore;
  }

  /**
   * Creates the inventory info and adds it to a HBox.
   *
   * @return the HBox with the inventory info
   */
  private HBox createInventoryInfo() {
    Label inventory = new Label("Inventar:");
    Label inventoryValue = new Label(player.getInventoryToString());
    InventoryGoal goal = (InventoryGoal) getGoal("InventoryGoal");
    Label inventoryGoal = new Label(goal.getMandatoryItemsToString());
    Label inventoryGoalAchieved = new Label();
    setGoalAchievedText(goal, inventoryGoalAchieved);

    inventory.setWrapText(true);
    inventoryValue.setWrapText(true);
    inventoryGoal.setWrapText(true);
    inventoryGoalAchieved.setWrapText(true);

    addStyleSheetToLabel(inventory, inventoryValue, inventoryGoal, inventoryGoalAchieved);

    HBox hBoxInventory = new HBox(inventory, inventoryValue, inventoryGoal, inventoryGoalAchieved);
    hBoxInventory.getStyleClass().add("hBoxInventory");
    return hBoxInventory;
  }

  /**
   * Creates the End scene.
   */
  private void createScene() {
    VBox vBox = new VBox();
    vBox.getChildren().addAll(createTitle(), createButton(), createHeadings(),
        createGoldInfo(), createHealthInfo(), createScoreInfo(), createInventoryInfo());
    scene = new Scene(vBox, 720, 670);

    String cssPath = Objects.requireNonNull(getClass().getResource("/Stylesheets/EndStyle.css")).toExternalForm();
    scene.getStylesheets().add(cssPath);
    vBox.getStyleClass().add("vBox");
  }
}
