package edu.ntnu.idatt2001.io;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Story;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

/**
 * The class is used for creating new files and writing stories to files.
 */
public class StoryFileWriter {

  /**
   * Create file.
   *
   * @param fileName the file name
   */
  public static void createFile(String fileName) {
    File file = new File(fileName);
    try {
      if (file.createNewFile()) {
        System.out.println("File created: " + file.getName());
      } else {
        System.out.println("File already exists.");
      }
    } catch (IOException e) {
      System.out.println("An error occurred while creating the file.");
      e.printStackTrace();
    }
  }

  /**
   * Write story to file.
   *
   * @param fileName the file name
   * @param story    the story
   */
  public static void writeStory(String fileName, Story story) {
    try {
      FileWriter writer = new FileWriter(fileName);
      writer.write(story.getTitle());
      writePassage(writer, story.getOpeningPassage());
      Collection<Passage> passages = story.getPassages();
      passages.remove(story.getOpeningPassage());

      for (Passage passage : passages) {
        writePassage(writer, passage);
      }

      writer.close();
    } catch (IOException e) {
      System.out.println("An error occurred while writing to the file.");
      e.printStackTrace();
    }
  }


  /**
   * Write passage to file.
   *
   * @param writer the file writer
   * @param passage the passage
   */
  private static void writePassage(FileWriter writer, Passage passage) {
    try {
      writer.write(System.lineSeparator());
      writer.write(System.lineSeparator());
      writer.write("::" + passage.getTitle());
      writer.write(System.lineSeparator());
      writer.write(passage.getContent());
      for (Link link : passage.getLinks()) {
        writer.write(System.lineSeparator());
        writer.write("[" + link.getText() + "](" + link.getReference() + ")");
      }

    } catch (IOException e) {
      System.out.println("An error occurred while writing to the file.");
      e.printStackTrace();
    }
  }
}
