package edu.ntnu.idatt2001.io;

import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Story;
import edu.ntnu.idatt2001.Models.actions.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang3.StringUtils;

/**
 * The class is used to read from a file.
 */
public class StoryFileReader {

  /**
   * Check if the file is a ".paths" or ".txt"-file
   *
   * @param file the file
   * @throws IOException the io exception
   */
  public static void checkFile(File file) throws IOException {
    if (!file.getName().endsWith(".paths") && !file.getName().endsWith(".txt")) {
      throw new IOException("Unsupported file format. Only .paths and .txt-files are supported.");
    }

    Scanner sc = new Scanner(file, StandardCharsets.UTF_8);


    StringBuilder content = new StringBuilder();

    while (sc.hasNextLine()) {
      String line = sc.nextLine().trim();
      content.append(line);
    }
    if (content.isEmpty()) {
      throw new IOException("The file is empty.");
    }
  }

  /**
   * Gets story title.
   *
   * @param file the file
   * @return the story title
   * @throws IOException the io exception
   */
  public static String getStoryTitle(File file) throws IOException {
    checkFile(file);

    Scanner sc = new Scanner(file, StandardCharsets.UTF_8);


    String title = "";
    boolean run = true;
    while (sc.hasNextLine() && run) {
      String line = sc.nextLine().trim();
      if (!line.isEmpty()) {
        title = line;
        run = false;
      }
    }

    return title;
  }

  /**
   * Collects all the passages from the file.
   *
   * @param file the file
   * @return the passages
   * @throws IOException the io exception
   */
  public static ArrayList<Passage> getPassages(File file) throws IOException {
    checkFile(file);
    Scanner sc = new Scanner(file, StandardCharsets.UTF_8);


    ArrayList<String> textPassages = new ArrayList<>();

    sc.useDelimiter("::");
    while (sc.hasNext()) {
      String textPassage = sc.next();
      textPassages.add(textPassage.strip());
    }
    sc.close();
    textPassages.remove(0); //fjerner story title


    ArrayList<Passage> passages = new ArrayList<>();
    for (String textPassage : textPassages) {
      Passage passage = getPassage(textPassage);
      passages.add(passage);
    }

    return passages;
  }

  /**
   * Creates a passage out of the string that has information about the passage.
   *
   * @param textPassage the string with information about the passage
   * @return the passage
   * @throws IOException the io exception
   */
  public static Passage getPassage(String textPassage) throws IOException {
    String title = getPassageTitle(textPassage).toLowerCase();
    String content = getPassageContent(textPassage);
    ArrayList<Link> links = getLinks(textPassage);
    return new Passage(title, content, links);
  }

  /**
   * Gets passage title.
   *
   * @param textPassage the string with information about the passage
   * @return the passage title
   * @throws IOException the io exception
   */
  public static String getPassageTitle(String textPassage) throws IOException {
    BufferedReader reader = new BufferedReader(new StringReader(textPassage));
    return reader.readLine();
  }

  /**
   * Gets passage content.
   *
   * @param textPassage the string with information about the passage
   * @return the passage content
   */
  public static String getPassageContent(String textPassage) {
    String content = StringUtils.substringBetween(textPassage, "\n", "[");
    if (!textPassage.contains("[")) {
      int position = textPassage.indexOf('\n');
      content = textPassage.substring(position);
    }
    return content.trim();
  }

  /**
   * Gets the links in the textPassage. And adds the connected actions to the links.
   *
   * @param textPassage the string with information about the passage
   * @return the links
   */
  public static ArrayList<Link> getLinks(String textPassage) {
    List<String> linkText = getLinkOrActionInfo(textPassage, "[", "]");
    List<String> linkReference = getLinkOrActionInfo(textPassage, "(", ")");

    ArrayList<Link> links = new ArrayList<>();
    for (int i = 0; i < linkText.size(); i++) {
      Link link = new Link(linkText.get(i), linkReference.get(i));

      //add actions to links:
      List<List<Object>> actions = getActions(textPassage);
      for (List<Object> linkAndAction : actions) {
        String l = (String) linkAndAction.get(0);
        Action action = (Action) linkAndAction.get(1);

        if (link.getReference().equals(l)) {
          link.addAction(action);
        }
      }

      links.add(link);
    }
    return links;
  }


  /**
   * Gets the actions of all the links in the textPassage.
   * If the action is declared with an invalid type in the file, the action is not added to the list.
   *
   * @param textPassage the string with information about the passage
   * @return a list of lists with link reference and action connected
   */
  public static List<List<Object>> getActions(String textPassage) {
    List<String> actionsInfo = getLinkOrActionInfo(textPassage, "{", "}");

    List<List<Object>> nested = new ArrayList<>();
    for (String s : actionsInfo) {
      String[] info = s.split(",");
      String link = info[0].trim();
      String actionType = info[1].trim();
      String actionValue = info[2].trim();

      switch (actionType.toLowerCase()) {
        case "g" -> {
          GoldAction goldAction = new GoldAction(Integer.parseInt(actionValue));
          addLinkAndAction(link, goldAction, nested);
        }
        case "h" -> {
          HealthAction healthAction = new HealthAction(Integer.parseInt(actionValue));
          addLinkAndAction(link, healthAction, nested);
        }
        case "s" -> {
          ScoreAction scoreAction = new ScoreAction(Integer.parseInt(actionValue));
          addLinkAndAction(link, scoreAction, nested);
        }
        case "i" -> {
          InventoryAction inventoryAction = new InventoryAction(actionValue);
          addLinkAndAction(link, inventoryAction, nested);
        }
      }
    }
    return nested;
  }

  /**
   * Adds linkReference and action to a list, and puts this list in the nested list.
   *
   * @param linkReference the link reference
   * @param action the action
   * @param nested the nested list
   */
  private static void addLinkAndAction(String linkReference, Action action, List<List<Object>> nested) {
    List<Object> linkAndAction = new ArrayList<>();
    linkAndAction.add(linkReference);
    linkAndAction.add(action);
    nested.add(linkAndAction);
  }

  /**
   * Gets the content or reference of all the links of a passage.
   * Or gets the information about all the actions of a passage.
   * To get the link contents, the start delimiter is "[" and the end delimiter is "]".
   * To get the link references, the start delimiter is "(" and the end delimiter is ")".
   * To get the action information, the start delimiter is "{" and the end delimiter is "}".
   *
   * @param textPassage    the string with information about the passage
   * @param startDelimiter the start delimiter
   * @param endDelimiter   the end delimiter
   * @return the link content or reference
   */
  public static ArrayList<String> getLinkOrActionInfo(String textPassage, String startDelimiter, String endDelimiter) {
    ArrayList<String> linkInfo = new ArrayList<>();
    int startIndex = 0;

    String substring = "";
    while ((substring = StringUtils.substringBetween(textPassage, startDelimiter, endDelimiter)) != null) {
      linkInfo.add(substring.toLowerCase());
      startIndex = textPassage.indexOf(endDelimiter) + 1;

      textPassage = textPassage.substring(startIndex);
    }
    return linkInfo;
  }

  /**
   * Gets story.
   *
   * @param file the file
   * @return the story
   * @throws IOException the io exception
   */
  public static Story getStory(File file) throws IOException {
    String title = getStoryTitle(file);
    ArrayList<Passage> passages = getPassages(file);
    Story story = new Story(title, passages.get(0));

    for (int i = 1; i < passages.size(); i++) {
      story.addPassage(passages.get(i));
    }
    return story;
  }
}