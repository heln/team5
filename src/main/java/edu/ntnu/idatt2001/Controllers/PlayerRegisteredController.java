package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Views.PassageView;
import edu.ntnu.idatt2001.Views.PlayerRegisteredView;
import java.io.IOException;
import javafx.stage.Stage;

/**
 * Player registered controller.
 * Adds functionality to the Player registered view.
 */
public class PlayerRegisteredController {
  private final PlayerRegisteredView view;
  private final Stage stage;


  /**
   * Instantiates a new Player registered controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public PlayerRegisteredController(PlayerRegisteredView view, Stage stage) {
    this.view = view;
    this.stage = stage;
  }

  /**
   * Handle main menu button clicked.
   * Go to main menu.
   *
   * @throws IOException the io exception
   */
  public void handleMainMenuButtonClicked() throws IOException {
    stage.setScene(view.getMainMenuView().getScene());
  }

  /**
   * Handle start game button clicked.
   * Go to first passage.
   */
  public void handleStartGameButtonClicked() {
    PassageView passageView = new PassageView(stage, view.getMainMenuView(), view.getGame());
    stage.setScene(passageView.getScene());
  }
}
