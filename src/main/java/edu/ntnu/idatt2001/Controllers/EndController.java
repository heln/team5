package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Views.EndView;
import javafx.stage.Stage;

/**
 * This class handels the user events of the End scene.
 */
public class EndController {
  private final Stage stage;
  private final EndView view;

  /**
   * Instantiates a new End controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public EndController(EndView view, Stage stage) {
    this.stage = stage;
    this.view = view;
  }

  /**
   * Handles main menu button so that the scene changes to the main menu scene.
   */
  public void handleMainMenuButton() {
    stage.setScene(view.getMainMenuView().getScene());
  }
}
