package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Models.Game;
import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.goals.*;
import edu.ntnu.idatt2001.Views.CreatePlayerView;
import edu.ntnu.idatt2001.Views.PlayerRegisteredView;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

/**
 * Create player controller.
 * Adds functionality to the Create player view.
 */
public class CreatePlayerController {
  private final CreatePlayerView view;
  private final Stage stage;
  private Player player;
  private List<String> inputInventoryGoal;
  private List<Goal> goals;

  /**
   * Instantiates a new Create player controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public CreatePlayerController(CreatePlayerView view, Stage stage) {
    this.view = view;
    this.stage = stage;
    inputInventoryGoal = new ArrayList<>();
  }

  /**
   * Gets player.
   *
   * @return the player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Gets goals.
   *
   * @return the goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * Gets input inventory goal.
   *
   * @return the input inventory goal
   */
  public List<String> getInputInventoryGoal() {
    return inputInventoryGoal;
  }

  /**
   * Handle main menu button.
   * Change scene to the main menu.
   *
   * @throws FileNotFoundException the file not found exception
   */
  public void handleMainMenuButton() throws FileNotFoundException {
    stage.setScene(view.getMainMenuView().getScene());
  }

  /**
   * Create a player object from the user input.
   * Set the goals for the player.
   */
  private void createPlayerAndGoals() {
    String inputName = view.getNameTextField().getText();
    int inputGold = view.getGoldSpinner().getValue(); //kan starte med null
    int inputHealth = view.getHealthSpinner().getValue(); //kan starte med null
    int inputGoldGoal = view.getGoldGoalSpinner().getValue();
    int inputHealthGoal = view.getHealthGoalSpinner().getValue();
    int inputScoreGoal = view.getScoreGoalSpinner().getValue();

    player = new Player.PlayerBuilder()
        .name(inputName)
        .gold(inputGold)
        .health(inputHealth)
        .build();

    goals = new ArrayList<>();
    goals.add(new GoldGoal(inputGoldGoal));
    goals.add(new HealthGoal(inputHealthGoal));
    goals.add(new ScoreGoal(inputScoreGoal));
    goals.add(new InventoryGoal(inputInventoryGoal));
  }

  /**
   * Handle create player button clicked.
   * Create a new game and moves to another scene.
   */
  public void handleCreatePlayerButtonClicked() {
    createPlayerAndGoals();
    Game game = new Game(player, view.getMainMenuView().getStory(), goals);

    PlayerRegisteredView playerRegisteredView = new PlayerRegisteredView(stage, game, view.getMainMenuView());
    stage.setScene(playerRegisteredView.getScene());
  }

  /**
   * Handle checkbox action.
   *
   * @param checkbox the checkbox
   * @param selected if the checkbock is selected
   */
  public void handleCheckboxAction(CheckBox checkbox, boolean selected) {
    String tekst = checkbox.getText();
    if (selected) {
      System.out.println("Avkrysningsboks valgt: " + tekst);
      inputInventoryGoal.add(checkbox.getText());
    } else {
      System.out.println("Avkrysningsboks fjernet valg: " + tekst);
      inputInventoryGoal.remove(checkbox.getText());
    }
  }
}
