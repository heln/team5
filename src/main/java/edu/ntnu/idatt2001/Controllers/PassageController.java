package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Models.Game;
import edu.ntnu.idatt2001.Models.Link;
import edu.ntnu.idatt2001.Models.Passage;
import edu.ntnu.idatt2001.Models.Player;
import edu.ntnu.idatt2001.Models.actions.Action;
import edu.ntnu.idatt2001.Models.actions.ScoreAction;
import edu.ntnu.idatt2001.Models.goals.*;
import edu.ntnu.idatt2001.Views.EndView;
import edu.ntnu.idatt2001.Views.PassageView;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * Passage controller.
 * Adds functionality to the Passage view.
 */
public class PassageController {
  private final PassageView view;
  private final Stage stage;
  private Set<Goal> fulfilledGoals;
  private Game game;


  /**
   * Instantiates a new Passage controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public PassageController(PassageView view, Stage stage) {
    this.view = view;
    this.stage = stage;
    game = view.getGame();
    fulfilledGoals = new HashSet<>();
  }

  /**
   * Check if goals are fulfilled.
   */
  private void checkIfGoalsAreFulfilled() {
    view.getMessage().setText("");

    for (Goal goal : game.getGoals()) {
      handleGoalFulfilled(goal, game.getPlayer());
    }
  }

  /**
   * Handle when goals are fulfilled.
   * Print message to user.
   *
   * @param goal   the goal
   * @param player the player
   */
  private void handleGoalFulfilled(Goal goal, Player player) {
    if (!fulfilledGoals.contains(goal) && goal.isFulfilled(player)) {
      fulfilledGoals.add(goal);

      String text;
      switch (goal.getClass().getSimpleName()) {
        case "ScoreGoal" -> text = "Poengmålet er oppnådd! +100 poeng";
        case "HealthGoal" -> text = "Helsemålet er oppnådd! +100 poeng";
        case "GoldGoal" -> text = "Gullmålet er oppnådd! +100 poeng";
        case "InventoryGoal" -> text = "Inventarmålet er oppnådd! +100 poeng";
        default -> throw new IllegalArgumentException("Unexpected goal type: " + goal.getClass().getSimpleName());
      }
      Label message = view.getMessage();
      view.getMessage().setText(message.getText() + "\n" + text);

      //add points
      ScoreAction scoreAction = new ScoreAction(100);
      scoreAction.execute(player);
    }
  }

  /**
   * Execute actions.
   *
   * @param link   the connected link
   * @param player the player
   */
  private void executeActions(Link link, Player player) {
    List<Action> actions = link.getActions();
    for (Action action : actions) {
      action.execute(player);
    }
  }


  /**
   * Handle link button.
   * Execute actions, check goals,
   * and update player information.
   *
   * @param link   the link
   * @param player the player
   */
  public void handleLinkButton(Link link, Player player) {
    Passage passage = game.go(link);

    executeActions(link, player);
    checkIfGoalsAreFulfilled();

    updatePlayerInfoValues(player);
    updatePassageInfo(passage);
  }

  /**
   * Update player information.
   *
   * @param player the player
   */
  private void updatePlayerInfoValues(Player player) {
    view.getGoldValue().setText(Integer.toString(player.getGold()));
    view.getHealthValue().setText(Integer.toString(player.getHealth()));
    view.getScoreValue().setText(Integer.toString(player.getScore()));
    view.getInventoryValue().setText(player.getInventoryToString());
  }

  /**
   * Update passage information.
   *
   * @param passage the chosen passage
   */
  private void updatePassageInfo(Passage passage) {
    String title = view.firstLetterToUppercase(passage.getTitle());
    view.getTitle().setText(title);
    view.getContent().setText(passage.getContent());
  }

  /**
   * Handle end passage.
   */
  public void handleEndPassage() {
    EndView endView = new EndView(stage, view.getMainMenuView(), game);
    stage.setScene(endView.getScene());
  }

  /**
   * Handle main menu button.
   * Change scene to main menu.
   */
  public void handleMainMenuButton() {
    stage.setScene(view.getMainMenuView().getScene());
  }

  /**
   * Handle restart button.
   */
  public void handleRestartButton() {
    Player player = view.resetPlayer();
    fulfilledGoals.clear();
    updatePlayerInfoValues(player);

    updatePassageInfo(game.begin());
  }

  /**
   * Handle how to button.
   * Show dialog-box with instructions.
   */
  public void handleHowToButton() {
    view.getDialog().showAndWait();
  }

  /**
   * Show alert box.
   *
   * @param iae the iae
   */
  public void showAlertBox(IllegalArgumentException iae) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Feil med link");
    alert.setContentText(iae.getMessage() + "\nDette gjør at knappen ikke fungerer.");
    alert.show();
  }
}
