package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Models.Story;
import edu.ntnu.idatt2001.Views.CreatePlayerView;
import edu.ntnu.idatt2001.Views.InstructionsView;
import edu.ntnu.idatt2001.Views.MainMenuView;
import edu.ntnu.idatt2001.io.StoryFileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

/**
 * Main menu controller.
 * Adds functionality to the Main menu view.
 */
public class MainMenuController {

  private final MainMenuView view;
  private final Stage stage;

  /**
   * Instantiates a new Main menu controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public MainMenuController(MainMenuView view, Stage stage) {
    this.view = view;
    this.stage = stage;
  }

  /**
   * Set up table with existing files from the Stories folder.
   *
   * @throws IOException the io exception
   */
  public void setUpTable() throws IOException {
    ObservableList<FileData> files = FXCollections.observableArrayList();

    File theLostRandomneeSki = new File("src/main/resources/stories/Mistet-Skien-i-Havet.paths");
    Story theLostSkiStory = StoryFileReader.getStory(theLostRandomneeSki);
    String fileTitle1 = theLostSkiStory.getTitle();
    String filePath1 = theLostRandomneeSki.getPath();
    int brokenLinks1 = theLostSkiStory.getBrokenLinks().size();

    FileData lostSki1 = new FileData(fileTitle1, filePath1, brokenLinks1);
    files.add(lostSki1);

    view.getTableView().setItems(files);
  }

  /**
   * Handle upload file button click.
   *
   * @param selectedFile the selected file
   * @throws IOException the io exception
   */
  public void handleUploadFileButtonClick(File selectedFile) throws IOException {
    if (selectedFile != null) {
      addFileToTable(selectedFile);
    }
  }

  /**
   * Add a chosen file from the computer to the table.
   *
   * @param file the chosen file
   * @throws IOException the io exception
   */
  private void addFileToTable(File file) throws IOException {
    ObservableList<FileData> files = view.getTableView().getItems();

    Story selectedStory = StoryFileReader.getStory(file); //her kastes det

    String fileTitle = selectedStory.getTitle();
    String filePath = file.getPath();
    int brokenLinks = selectedStory.getBrokenLinks().size();

    FileData fileData = new FileData(fileTitle, filePath, brokenLinks);
    files.add(fileData);
  }

  /**
   * The type File data.
   */
  public static class FileData {
    private final String fileName;
    private final String location;
    private final int brokenLinks;
    private final File file;

    /**
     * Instantiates a new File data.
     *
     * @param fileName    the file name
     * @param location    the location
     * @param brokenLinks the broken links
     */
    public FileData(String fileName, String location, int brokenLinks) {
      this.fileName = fileName;
      this.location = location;
      this.brokenLinks = brokenLinks;
      this.file = new File(location);
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    public String getFileName() {
      return fileName;
    }

    /**
     * Gets location.
     *
     * @return the location
     */
    public String getLocation() {
      return location;
    }

    /**
     * Gets broken links.
     *
     * @return the broken links
     */
    public int getBrokenLinks() {
      return brokenLinks;
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public File getFile() {
      return file;
    }
  }

  /**
   * Handle create player button click.
   * Take the user to another view.
   *
   */
  public void handleCreatePlayerButtonClick() {
    CreatePlayerView createPlayerView = new CreatePlayerView(stage, view);
    stage.setScene(createPlayerView.getScene());
  }

  /**
   * Handle settings button click.
   * Take the user to another view.
   *
   * @throws FileNotFoundException the file not found exception
   */
  public void handleSettingsButtonClick() throws IOException {
    InstructionsView instructionsView = new InstructionsView(stage, view);
    stage.setScene(instructionsView.getScene());
  }
}
