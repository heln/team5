package edu.ntnu.idatt2001.Controllers;

import edu.ntnu.idatt2001.Views.InstructionsView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import javafx.stage.Stage;

/**
 * Instructions controller.
 * Adds functionality to the Instructions view.
 */
public class InstructionsController {
  private final InstructionsView view;
  private Stage stage;

  /**
   * Instantiates a new Instructions controller.
   *
   * @param view  the view
   * @param stage the stage
   */
  public InstructionsController(InstructionsView view, Stage stage) {
    this.view = view;
    this.stage = stage;
  }

  /**
   * Reads the instructions from a file.
   *
   * @param file the file
   * @return the instructions
   * @throws IOException IOException
   */
  public String writeInstructions(File file) throws IOException {
    Scanner sc = new Scanner(file, StandardCharsets.UTF_8);

    StringBuilder instructions = new StringBuilder();

    while (sc.hasNextLine()) {
      String line = sc.nextLine().trim();
      instructions.append(line).append("\n");
    }
    return instructions.toString();
  }

  /**
   * Handle main menu button clicked.
   * Change scene to main menu.
   *
   * @throws FileNotFoundException the file not found exception
   */
  public void handleMainMenuButtonClicked() throws FileNotFoundException {
    stage.setScene(view.getMainMenuView().getScene());
  }
}
